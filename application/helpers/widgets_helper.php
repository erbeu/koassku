<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function alert($type, $msg)
{
    $alert = <<<EOT
<div class="alert alert-{$type} alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {$msg}
</div>
EOT;

    return $alert;
}
