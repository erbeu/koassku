<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function activate_menu($controller, $method = null)
{
    $CI = get_instance();
    $class = $CI->router->fetch_class();

    if ($method != null) {
        return ($class == $controller && $CI->router->fetch_method() == $method) ? 'active' : '';
    } else {
        return ($class == $controller) ? 'active' : '';
    }
}
