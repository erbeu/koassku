<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push('Keluhan', $this->router->fetch_module().'/keluhan/');
$this->breadcrumbs->push($title, $this->router->fetch_module().'/keluhan/detail/'.$keluhan->id_keluhan);
?>

<h3><i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <?= $title ?></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<?= $this->session->flashdata('message') != null ? $this->session->flashdata('message') : '' ?>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default panel-profil">
            <div class="panel-body">
                <img src="<?= base_url('_uploads/pasien/'.$keluhan->foto) ?>" alt="Foto" class="img-responsive img-center img-small img-thumbnail">
                <div class="text-center">
                    <h4><?= $keluhan->nama_pasien ?></h4>
                    <small><?= $this->config->item('data_provider')['jenis_kelamin'][$keluhan->jk] ?></small>
                    <span class="label label-primary label-role">PASIEN</span>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <table class="table table-bordered">
                <tr>
                    <td>Konsultan</td>
                    <td>
                        <?= $keluhan->nama_konsultan ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%">Subjek</td>
                    <td>
                        <?= $keluhan->subjek ?>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>
                        <?= $this->config->item('data_provider')['jenis_kelamin'][$keluhan->jenis_kelamin] ?>
                    </td>
                </tr>
                <tr>
                    <td>Usia</td>
                    <td>
                        <?= $this->config->item('data_provider')['usia'][$keluhan->usia] ?>
                    </td>
                </tr>
                <tr>
                    <td>Bagian Sakit</td>
                    <td>
                        <?= $keluhan->bagian_sakit ?>
                    </td>
                </tr>
                <tr>
                    <td>Lama Sakit</td>
                    <td>
                        <?= $keluhan->lama_sakit ?>
                    </td>
                </tr>
                <tr>
                    <td>Menggunakan Obat Kumur</td>
                    <td>
                        <?= $this->config->item('data_provider')['ya_tidak'][$keluhan->obat_kumur] ?>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Bulu Sikat</td>
                    <td>
                        <?= $this->config->item('data_provider')['jenis_bulu_sikat'][$keluhan->jenis_bulu_sikat] ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p>Foto Gigi</p>
                        <?php if ($keluhan->foto_gigi == null) : ?>
                            <p>Tidak ada foto</p>
                        <?php else : ?>
                            <img src="<?= base_url('_uploads/gigi/'.$keluhan->foto_gigi) ?>" alt="Foto Gigi" class="img-responsive">
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>

        <a href="<?= site_url($this->router->fetch_module().'/keluhan/') ?>" class="btn btn-default btn-block"><i class="fa fa-chevron-left fa-fw" aria-hidden="true"></i> Kembali</a>
    </div>

    <div class="col-md-8">
        <div class="panel panel-default message-area">
            <div class="row">
                <div class="col-md-8">
                    <div class="well">
                        <b><?= $keluhan->nama_pasien ?></b>
                        <br>
                        <?= $keluhan->deskripsi ?>
                        <br>
                        <small class="momentjs label label-info"><?= $keluhan->created_at ?></small>
                    </div>
                </div>
            </div>

            <?php foreach ($keluhan_detail as $data) : ?>
                <div class="row">
                    <div class="col-md-8 <?= $data->pengirim == 2 ? 'col-md-offset-4' : '' ?>">
                        <div class="well">
                            <b><?= $data->pengirim == 2 ? 'Anda' : $keluhan->nama_pasien ?></b>
                            <br>
                            <?= $data->isi ?>
                            <br>
                            <small class="momentjs label label-info"><?= $data->created_at ?></small>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <hr>

            <form action="<?= site_url($this->router->fetch_module().'/keluhan/balas/'.$keluhan->id_keluhan) ?>" method="post">
                <div class="form-group">
                    <label for="isi"><?= form_error('isi') ?></label>
                    <textarea name="isi" id="isi" rows="5" placeholder="Tulis pesan anda disini..."><?= set_value('isi') ?></textarea>
                </div>

                <?php if (count($jadwal) == 0) : ?>
                   <button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-calendar"></span> Buat Jadwal</button>
                <?php else : ?>
                    <a href="<?= site_url($this->router->fetch_module().'/jadwal/detail/'.$jadwal[0]->id_jadwal) ?>" class="btn btn-danger pull-left"><span class="glyphicon glyphicon-calendar"></span> Lihat Jadwal</a>
                <?php endif; ?>
                <button type="submit" name="submit" class="btn btn-primary pull-right" value="simpan">Kirim Pesan <span class="glyphicon glyphicon-chevron-right"></span></button>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>

<?php if (count($jadwal) == 0) : ?>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="<?= base_url($this->router->fetch_module().'/keluhan/buat_jadwal/'.$keluhan->id_keluhan) ?>" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Buat Jadwal</h4>
                    </div>
                    <div class="modal-body">
                            <div class="form-group">
                                <label for="judul">Judul <?= form_error('judul') ?></label>
                                <input type="text" name="judul" class="form-control" id="judul" placeholder="Judul" value="<?= set_value('judul') ?>">
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi <?= form_error('deskripsi') ?></label>
                                <textarea name="deskripsi" class="form-control" id="deskripsi" rows="3" placeholder="Deskripsi"><?= set_value('deskripsi') ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="waktu">Waktu <?= form_error('waktu') ?></label>
                                <input type="text" name="waktu" class="form-control datetimepicker" id="waktu" placeholder="Waktu" value="<?= set_value('waktu') ?>">
                            </div>
                            <div class="form-group">
                                <label for="lokasi">Lokasi <?= form_error('lokasi') ?></label>
                                <textarea name="lokasi" class="form-control" id="lokasi" rows="3" placeholder="Lokasi"><?= set_value('lokasi') ?></textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary" name="submit" value="simpan">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
