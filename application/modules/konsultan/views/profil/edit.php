<h3><?= $title ?></h3>
<hr>

<?= $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

<form action="" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="email">Email <?= form_error('email') ?></label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="<?= set_value('email', $konsultan->email) ?>">
            </div>
            <div class="form-group">
                <label for="password">Password <?= form_error('password') ?></label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="">
                <small class="help-text">Kosongkan jika tidak ingin mengganti password.</small>
            </div>
            <div class="form-group">
                <label for="nama">Nama Lengkap <?= form_error('nama_konsultan') ?></label>
                <input type="text" name="nama_konsultan" class="form-control" id="nama" placeholder="Nama Lengkap" value="<?= set_value('nama_konsultan', $konsultan->nama_konsultan) ?>">
            </div>
            <div class="form-group">
                <label for="no_mahasiswa">No Mahasiswa <?= form_error('no_mahasiswa') ?></label>
                <input type="text" name="no_mahasiswa" class="form-control" id="no_mahasiswa" placeholder="No Mahasiswa" value="<?= set_value('no_mahasiswa', $konsultan->no_mahasiswa) ?>">
            </div>
            <div class="form-group">
                <label for="no_ktp">No KTP <?= form_error('no_ktp') ?></label>
                <input type="text" name="no_ktp" class="form-control" id="no_ktp" placeholder="No KTP" value="<?= set_value('no_ktp', $konsultan->no_ktp) ?>">
            </div>
            <div class="form-group">
                <label for="no_hp">No Handphone <?= form_error('no_hp') ?></label>
                <input type="text" name="no_hp" class="form-control" id="no_hp" placeholder="No Handphone" value="<?= set_value('no_hp', $konsultan->no_hp) ?>">
            </div>
            <div class="form-group">
                <label for="alamat">Alamat <?= form_error('alamat') ?></label>
                <textarea name="alamat" class="form-control" id="alamat" rows="3" placeholder="Alamat"><?= set_value('alamat', $konsultan->alamat) ?></textarea>
            </div>
            <div class="form-group">
                <label for="kotakab">Kota/Kab <?= form_error('kotakab') ?></label>
                <select name="kotakab" id="kotakab" class="form-control select2">
                    <option value="">- Pilih Kota/Kab -</option>
                    <?php foreach($list_kotakab as $data) : ?>
                        <option value="<?= $data->id_kotakab ?>" <?= set_value('kotakab', $konsultan->kotakab) == $data->id_kotakab ? 'selected' : '' ?>>
                            <?= $data->nama_kotakab ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="tempat_lahir">Tempat, Tanggal Lahir <?= form_error('tempat_lahir') ?> <?= form_error('tanggal_lahir') ?></label>
                <div class="row">
                    <div class="col-md-6">
                        <select name="tempat_lahir" id="tempat_lahir" class="form-control select2">
                            <option value="">- Pilih Tempat Lahir -</option>
                            <?php foreach($list_kotakab as $data) : ?>
                                <option value="<?= $data->id_kotakab ?>" <?= set_value('tempat_lahir', $konsultan->tempat_lahir) == $data->id_kotakab ? 'selected' : '' ?>>
                                    <?= $data->nama_kotakab ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="tanggal_lahir" class="form-control datetimepicker" id="tanggal_lahir" placeholder="Tanggal Lahir" value="<?= set_value('tanggal_lahir', $konsultan->tanggal_lahir) ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin <?= form_error('jenis_kelamin') ?></label>
                <select name="jenis_kelamin" id="jenis_kelamin" class="form-control select2">
                    <option value="">- Pilih Jenis Kelamin -</option>
                    <option value="L" <?= set_value('jenis_kelamin', $konsultan->jenis_kelamin) == 'L' ? 'selected' : '' ?>>Laki - Laki</option>
                    <option value="P" <?= set_value('jenis_kelamin', $konsultan->jenis_kelamin) == 'P' ? 'selected' : '' ?>>Perempuan</option>
                </select>
            </div>
            <div class="form-group">
                <label for="gol_darah">Gol Darah <?= form_error('gol_darah') ?></label>
                <select name="gol_darah" id="gol_darah" class="form-control select2">
                    <option value="">- Pilih Gol Darah -</option>
                    <?php foreach($list_gol_darah as $data) : ?>
                        <option value="<?= $data->id_gol_darah ?>" <?= set_value('gol_darah', $konsultan->gol_darah) == $data->id_gol_darah ? 'selected' : '' ?>>
                            <?= $data->nama_gol_darah ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="status_nikah">Status Nikah <?= form_error('status_nikah') ?></label>
                <select name="status_nikah" id="status_nikah" class="form-control select2">
                    <option value="">- Pilih Status Nikah -</option>
                    <?php foreach($list_status_nikah as $data) : ?>
                        <option value="<?= $data->id_status_nikah ?>" <?= set_value('status_nikah', $konsultan->status_nikah) == $data->id_status_nikah ? 'selected' : '' ?>>
                            <?= $data->nama_status_nikah ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="agama">Agama <?= form_error('agama') ?></label>
                <select name="agama" id="agama" class="form-control select2">
                    <option value="">- Pilih Agama -</option>
                    <?php foreach($list_agama as $data) : ?>
                        <option value="<?= $data->id_agama ?>" <?= set_value('agama', $konsultan->agama) == $data->id_agama ? 'selected' : '' ?>>
                            <?= $data->nama_agama ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="foto">Foto <?= form_error('foto') ?></label>
                <img src="<?= base_url('_uploads/konsultan/'.$konsultan->foto) ?>" alt="Foto" class="img-small img-center img-responsive"><br>
                <input type="file" name="foto" class="form-control" id="foto">
                <small class="help-text">Lewati jika tidak ingin mengganti foto.</small>
            </div>
        </div>
    </div>
    <button type="submit" name="submit" class="btn btn-primary" value="simpan">Simpan</button>
</form>
