<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push($title, $this->router->fetch_module().'/profil/');
?>

<h3><i class="fa fa-user fa-fw" aria-hidden="true"></i> <?= $title ?></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<?= $this->session->flashdata('message') != null ? $this->session->flashdata('message') : '' ?>

<div class="row">
    <div class="col-md-4">
        <?= $this->load->view('layouts/parts/panel-profil') ?>
    </div>

    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <table class="table">
                    <tr>
                        <td width="40%">Email</td>
                        <td>
                            <?= $konsultan->email ?>
                        </td>
                    </tr>
                    <tr>
                        <td>No Mahasiswa</td>
                        <td>
                            <?= $konsultan->no_mahasiswa ?>
                        </td>
                    </tr>
                    <tr>
                        <td>No KTP</td>
                        <td>
                            <?= $konsultan->email ?>
                        </td>
                    </tr>
                    <tr>
                        <td>No Handphone</td>
                        <td>
                            <?= $konsultan->no_hp ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>
                            <?= $konsultan->alamat ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Kota/Kab</td>
                        <td>
                            <?= $konsultan->kotakab ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <table class="table">
                    <tr>
                        <td width="40%">TTL</td>
                        <td>
                            <?= $konsultan->tempat_lahir ?>, <?= $konsultan->tanggal_lahir ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>
                            <?= $jenis_kelamin[$konsultan->jenis_kelamin] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Gol. Darah</td>
                        <td>
                            <?= $gol_darah[$konsultan->gol_darah] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Status Nikah</td>
                        <td>
                            <?= $status_nikah[$konsultan->status_nikah] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Agama</td>
                        <td>
                            <?= $agama[$konsultan->agama] ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<br>
<h4>Pasien Yang Ditangani</h4>
<hr>
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Waktu</th>
                <th>Nama Pasien</th>
                <th>Subjek</th>
                <th>Usia</th>
                <th>Kota/Kab</th>
                <th>Aksi</th>
            </tr>
            <tr>
                <form action="" method="get">
                    <td></td>
                    <td>
                        <input type="text" class="form-control input-sm" name="q[waktu]" placeholder="Cari Waktu" value="<?= $q['keluhan.created_at']; ?>">
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" name="q[nama_pasien]" placeholder="Cari Nama Pasien" value="<?= $q['nama_pasien']; ?>">
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" name="q[subjek]" placeholder="Cari Subjek" value="<?= $q['subjek']; ?>">
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" name="q[usia]" placeholder="Cari Usia" value="<?= $q['usia']; ?>">
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" name="q[nama_kotakab]" placeholder="Cari Kota/Kab" value="<?= $q['nama_kotakab']; ?>">
                    </td>
                    <td class="text-center">
                        <button type="submit" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></button>
                    </td>
                </form>
            </tr>
        </thead>
        <tbody>
            <?= (count($pasien) == 0) ? '<td colspan="7">Anda belum memiliki pasien</td>' : '' ?>
            <?php foreach ($pasien as $data) : ?>
                <tr>
                    <td>
                        <?= ++$start ?>
                    </td>
                    <td>
                        <?= $data->created_at ?>
                    </td>
                    <td>
                        <?= $data->nama_pasien ?>
                    </td>
                    <td>
                        <?= $data->subjek ?>
                    </td>
                    <td>
                        <?= $usia[$data->usia] ?>
                    </td>
                    <td>
                        <?= $data->nama_kotakab ?>
                    </td>
                    <td class="text-center">
                        <div class="btn-group btn-group-sm" role="group">
                            <?php
                            echo anchor(
                                site_url($this->router->fetch_module().'/keluhan/detail/'.$data->id_keluhan),
                                '<i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> Detail',
                                ['class' => 'btn btn-success']
                            );
                            ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-xs-6">
        Total Record :
        <?= $total_rows ?>
    </div>
    <div class="col-xs-6 text-right">
        <?= $pagination ?>
    </div>
</div>