<?php
$this->load->model('konsultan_model', 'konsultan');
$konsultan = $this->konsultan->get_by_id($this->session->id_konsultan);
?>

<div class="panel panel-default panel-profil">
    <div class="panel-body">
        <img src="<?= base_url('_uploads/konsultan/'.$konsultan->foto) ?>" alt="Foto" class="img-responsive img-center img-small img-thumbnail">
        <div class="text-center">
            <h4><?= $konsultan->nama_konsultan ?> <span class="label label-primary" data-toggle="tooltip" data-placement="top" title="Poin Anda"><?= $konsultan->poin ?></span></h4>
            <small><?= $this->config->item('data_provider')['jenis_kelamin'][$konsultan->jenis_kelamin] ?></small>
            <span class="label label-primary label-role">KONSULTAN</span>
        </div>
    </div>
</div>