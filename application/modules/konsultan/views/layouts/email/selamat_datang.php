<html>
<head>
    <title>Selamat Datang</title>
</head>
<body>
    <table cellpadding="0" cellspacing="0" style="max-width: 500px; width: 100%; margin-left: auto; margin-right: auto; font-family: Segoe UI; border: 1px solid #aaaaaa;">
        <tr>
            <td style="background-color: #2c3e50; color: #ffffff; padding: 20px; text-align: center; letter-spacing: -2px;">
                <h1>KOASSKU</h1>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px;">
                <p style="text-align: justify;">
                    Hai, <?= $nama_konsultan ?><br>
                    Selamat datang di Koassku. Untuk memulai konsultasi, kami harus memverifikasi data anda terlebih dahulu, dimohon untuk menunggu 1x24 jam.<br>
                    Apabila dalam waktu 1x24 jam anda belum mendapatkan balasan email, silahkan balas pesan ini.<br>
                    Terimakasih.
                </p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #f5f5f5; color: #aaaaaa; padding: 10px; text-align: center;">Pesan ini dikirim secara otomatis oleh sistem.</td>
        </tr>
    </table>
</body>
</html>
