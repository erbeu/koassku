<h1 class="text-center">PANEL LOGIN KONSULTAN</h1>

<?= $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

<form action="" method="post" class="form-signin">
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email Anda" value="<?= $data['email'] ?>" autofocus>
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password Anda">
    </div>
    <button type="submit" name="submit" value="1" class="btn btn-primary">Login</button>
</form>
<hr>
<p>Belum punya akun? <a href="<?= site_url($this->router->fetch_module().'/app/daftar/') ?>" class="btn btn-link">Daftar Sekarang!</a></p>
