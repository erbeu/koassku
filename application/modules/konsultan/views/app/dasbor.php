<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push($title, $this->router->fetch_module());
?>

<h3><i class="fa fa-home fa-fw" aria-hidden="true"></i> <?= $title ?> <small>Ikhtisar Aktivitas</small></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<div class="row">
    <div class="col-md-4">
        <?= $this->load->view('layouts/parts/panel-profil') ?>
    </div>

    <div class="col-md-8">
        <h4>Keluhan Baru Pasien</h4>
        <hr>
        <div class="table-wrapper">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Waktu</th>
                        <th>Nama Pasien</th>
                        <th>Subjek</th>
                        <th>Usia</th>
                        <th>Kota/Kab</th>
                        <th>Aksi</th>
                    </tr>
                    <tr>
                        <form action="<?= site_url('coass'); ?>" method="get">
                            <td></td>
                            <td>
                                <input type="text" class="form-control input-sm" name="q[waktu]" placeholder="Cari Waktu" value="<?= $q['keluhan.created_at']; ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" name="q[nama_pasien]" placeholder="Cari Nama Pasien" value="<?= $q['nama_pasien']; ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" name="q[subjek]" placeholder="Cari Subjek" value="<?= $q['subjek']; ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" name="q[usia]" placeholder="Cari Usia" value="<?= $q['usia']; ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" name="q[nama_kotakab]" placeholder="Cari Kota/Kab" value="<?= $q['nama_kotakab']; ?>">
                            </td>
                            <td class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search fa-fw" aria-hidden="true"></i></button>
                            </td>
                        </form>
                    </tr>
                </thead>
                <tbody>
                    <?= (count($keluhan) == 0) ? '<td colspan="7">Tidak ada keluhan</td>' : '' ?>
                    <?php foreach ($keluhan as $data) : ?>
                        <tr>
                            <td>
                                <?= ++$start ?>
                            </td>
                            <td class="momentjs">
                                <?= $data->created_at ?>
                            </td>
                            <td>
                                <?= $data->nama_pasien ?>
                            </td>
                            <td>
                                <?= $data->subjek ?>
                            </td>
                            <td>
                                <?= $this->config->item('data_provider')['usia'][$data->usia] ?>
                            </td>
                            <td>
                                <?= $data->nama_kotakab ?>
                            </td>
                            <td class="text-center">
                                <div class="btn-group btn-group-sm" role="group">
                                    <?php
                                    echo anchor(
                                        site_url($this->router->fetch_module().'/keluhan/tangani/'.$data->id_keluhan),
                                        '<i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> Tangani',
                                        [
                                            'class' => 'btn btn-success',
                                            'data-toggle' => 'tooltip',
                                            'data-placement' => 'top',
                                            'title' => 'Tangani keluhan ini'
                                        ]
                                    );
                                    ?>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-xs-6">
                Total Record :
                <?= $total_rows ?>
            </div>
            <div class="col-xs-6 text-right">
                <?= $pagination ?>
            </div>
        </div>
    </div>
</div>
