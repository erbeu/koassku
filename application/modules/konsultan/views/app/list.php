<h2>
    <?= $title ?>
    <?= anchor(
        site_url('admin/pertanyaan/create'),
        '<i class="glyphicon glyphicon-plus" aria-hidden="true"></i> Tambah',
        ['class' => 'btn btn-primary']
    ) ?>
</h2>

<div style="margin-top: 20px" id="message">
    <?= $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
</div>

<div class="table-wrapper">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width="50px">No</th>
                <th>Isi Pertanyaan</th>
                <th width="150px">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <form action="<?= site_url('admin/pertanyaan/index'); ?>" method="get">
                    <td></td>
                    <td><input type="text" class="form-control" name="q[isi_pertanyaan]" value="<?= $q['isi_pertanyaan']; ?>"></td>
                    <td class="text-center"><button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></button></td>
                </form>
            </tr>
            <?php foreach ($pertanyaan as $data) : ?>
                <tr>
                    <td><?= ++$start ?></td>
                    <td><?= $data->isi_pertanyaan ?></td>
                    <td class="text-center">
                        <div class="btn-group btn-group-sm" role="group">
                            <?php
                            echo anchor(
                                site_url('admin/pertanyaan/view/'.$data->id_pertanyaan),
                                '<i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>',
                                ['class' => 'btn btn-success']
                            );
                            echo anchor(
                                site_url('admin/pertanyaan/update/'.$data->id_pertanyaan),
                                '<i class="glyphicon glyphicon-edit" aria-hidden="true"></i>',
                                ['class' => 'btn btn-success']
                            );
                            echo anchor(
                                site_url('admin/pertanyaan/delete/'.$data->id_pertanyaan),
                                '<i class="glyphicon glyphicon-trash" aria-hidden="true"></i>',
                                [
                                    'class' => 'btn btn-success',
                                    'onclick' => 'javasciprt: return confirm(\'Anda yakin menghapus item ini?\')'
                                ]
                            );
                            ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-xs-6">
        Total Record : <?= $total_rows ?>
    </div>
    <div class="col-xs-6 text-right">
        <?= $pagination ?>
    </div>
</div>
