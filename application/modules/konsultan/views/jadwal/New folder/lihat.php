<h3><?= $title ?></h3>
<hr>

<?= $this->session->flashdata('message') != null ? $this->session->flashdata('message') : '' ?>

<div class="row">
    <div class="col-md-4">
        <div class="text-center">
            <img src="<?= base_url('_uploads/pasien/'.$keluhan->foto) ?>" alt="" class="img-rounded img-responsive img-center">
            <h4><?= $keluhan->nama_pasien ?></h4>
        </div>
        <table class="table table-bordered">
            <tr>
                <td width="50%">Email</td>
                <td width="50%">
                    <?= $keluhan->email ?>
                </td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>
                    <?= $keluhan->jenis_kelamin ?>
                </td>
            </tr>
            <tr>
                <td>Usia</td>
                <td>
                    <?= $keluhan->usia ?>
                </td>
            </tr>
            <tr>
                <td>Bagian Sakit</td>
                <td>
                    <?= $keluhan->bagian_sakit ?>
                </td>
            </tr>
            <tr>
                <td>Lama Sakit</td>
                <td>
                    <?= $keluhan->lama_sakit ?>
                </td>
            </tr>
            <tr>
                <td>Menggunakan Obat Kumur</td>
                <td>
                    <?= $keluhan->obat_kumur ?>
                </td>
            </tr>
            <tr>
                <td>Jenis Bulu Sikat</td>
                <td>
                    <?= $keluhan->jenis_bulu_sikat ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>Foto Gigi</p>
                    <?php if ($keluhan->foto_gigi == null) : ?>
                        <p>Tidak ada foto</p>
                    <?php else : ?>
                        <img src="<?= base_url('_uploads/gigi/'.$keluhan->foto_gigi) ?>" alt="Foto Gigi" class="img-responsive">
                    <?php endif; ?>
                </td>
            </tr>
        </table>
        <a href="<?= site_url($this->router->fetch_module().'/profil') ?>" class="btn btn-success btn-block"><span class="glyphicon glyphicon-chevron-left"></span> Kembali ke profil</a>
    </div>
    <div class="col-md-8">
        <table class="table table-bordered">
            <tr>
                <td width="30%">Judul</td>
                <td width="70%">
                    <?= $jadwal->judul ?>
                </td>
            </tr>
            <tr>
                <td>Deskripsi</td>
                <td>
                    <?= $jadwal->deskripsi ?>
                </td>
            </tr>
            <tr>
                <td>Waktu</td>
                <td>
                    <?= $jadwal->waktu ?>
                </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td>
                    <?= $jadwal->lokasi ?>
                </td>
            </tr>
        </table>
    </div>
</div>
