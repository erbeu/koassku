<h3><?= $title ?></h3>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Daftar Keluhan</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Waktu</th>
                                <th>Judul</th>
                                <th>Lokasi</th>
                                <th>Nama Pasien</th>
                                <th>Aksi</th>
                            </tr>
                            <tr>
                                <form action="" method="get">
                                    <td></td>
                                    <td>
                                        <input type="text" class="form-control input-sm" name="q[waktu]" placeholder="Cari Waktu" value="<?= $q['waktu']; ?>">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm" name="q[judul]" placeholder="Cari Judul" value="<?= $q['judul']; ?>">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm" name="q[lokasi]" placeholder="Cari Lokasi" value="<?= $q['lokasi']; ?>">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm" name="q[nama_pasien]" placeholder="Cari Nama Pasien" value="<?= $q['nama_pasien']; ?>">
                                    </td>
                                    <td class="text-center">
                                        <button type="submit" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></button>
                                    </td>
                                </form>
                            </tr>
                        </thead>
                        <tbody>
                            <?= (count($jadwal) == 0) ? '<td colspan="7">Anda belum memiliki jadwal</td>' : '' ?>
                                <?php foreach ($jadwal as $data) : ?>
                                    <tr>
                                        <td>
                                            <?= ++$start ?>
                                        </td>
                                        <td>
                                            <?= $data->waktu ?>
                                        </td>
                                        <td>
                                            <?= $data->judul ?>
                                        </td>
                                        <td>
                                            <?= $data->lokasi ?>
                                        </td>
                                        <td>
                                            <?= $data->nama_pasien ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group btn-group-sm" role="group">
                                                <?php
                                                echo anchor(
                                                    site_url($this->router->fetch_module().'/jadwal/lihat/'.$data->id_jadwal),
                                                    '<i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>',
                                                    ['class' => 'btn btn-success']
                                                );
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        Total Record :
                        <?= $total_rows ?>
                    </div>
                    <div class="col-xs-6 text-right">
                        <?= $pagination ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Kalender</h3>
            </div>
            <div class="panel-body">
                
            </div>
        </div>
    </div>
</div>