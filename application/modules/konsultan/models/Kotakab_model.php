<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Kotakab_model extends CI_Model
{
    public $table = 'kotakab';
    public $id = 'id_kotakab';
    public $order = 'ASC';

    public function __construct()
    {
        parent::__construct();
    }

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }
}
