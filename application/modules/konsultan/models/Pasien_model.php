<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pasien_model extends CI_Model
{
    public $table = 'pasien';
    public $id = 'id_pasien';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->select('pasien.id_pasien,
        pasien.email,
        pasien.nama_pasien,
        pasien.no_hp,
        pasien.alamat,
        pasien.tanggal_lahir,
        pasien.jenis_kelamin,
        pasien.foto,
        k1.nama_kotakab kotakab,
        k2.nama_kotakab tempat_lahir', false);
        $this->db->join('kotakab k1', 'k1.id_kotakab = pasien.kotakab', 'left');
        $this->db->join('kotakab k2', 'k2.id_kotakab = pasien.tempat_lahir', 'left');
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    // get data by id
    public function get_by_id_ori($id)
    {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    // get total rows
    public function total_rows($q = null)
    {
        $this->db->select('keluhan.id_keluhan,
        keluhan.jenis_kelamin,
        keluhan.usia,
        keluhan.subjek,
        keluhan.created_at,
        pasien.nama_pasien,
        kotakab.nama_kotakab', false);
        $this->db->join('pasien', 'pasien.id_pasien = keluhan.pasien', 'left');
        $this->db->join('kotakab', 'kotakab.id_kotakab = pasien.kotakab', 'left');
        $this->db->order_by($this->id, $this->order);

        if ($q != null) {
            $this->db->like($q);
        }

        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = null)
    {
        $this->db->select('keluhan.id_keluhan,
        keluhan.jenis_kelamin,
        keluhan.usia,
        keluhan.subjek,
        keluhan.created_at,
        pasien.nama_pasien,
        kotakab.nama_kotakab', false);
        $this->db->join('pasien', 'pasien.id_pasien = keluhan.pasien', 'left');
        $this->db->join('kotakab', 'kotakab.id_kotakab = pasien.kotakab', 'left');
        $this->db->order_by($this->id, $this->order);
        $this->db->like($q);
        $this->db->limit($limit, $start);

        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // update data
    public function decrease_point($id, $point)
    {
        $this->db->set('poin', 'poin-'.$point, FALSE);
        $this->db->where($this->id, $id);
        $this->db->update($this->table);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    // login action
    public function login($email, $password)
    {
        $this->db->where(['email' => $email, 'soft_delete' => 0]);
        $row = $this->db->get($this->table)->row();

        if (!empty($row)) {
            if ($row->status == 0) {
                return 3;
            }

            if (password_verify($password, $row->password)) {
                $this->session->set_userdata([
                    'id_pasien' => $row->id_pasien,
                    'nama_pasien' => $row->nama_pasien,
                    'email' => $row->email,
                    'role' => 'pasien',
                    'is_loggedin' => true,
                ]);

                return 1;
            }
        }

        return 2;
    }
    
    // search email
    public function search_email($email)
    {
        $this->db->where('email', $email);
        $this->db->where('id_pasien', $this->session->id_pasien);
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }
}
