<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Keluhan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('konsultan_model', 'konsultan');
        $this->load->model('keluhan_model', 'keluhan');
        $this->load->model('keluhan_detail_model', 'keluhan_detail');
        $this->load->model('jadwal_model', 'jadwal');
        $this->load->model('notif_model', 'notif');

        if (!$this->session->is_loggedin) {
            redirect(site_url($this->router->fetch_module().'/app/login'));
        }
    }

    public function index()
    {
        $q['keluhan.created_at'] = urldecode($this->input->get('q[waktu]', true));
        $q['nama_pasien'] = urldecode($this->input->get('q[nama_pasien]', true));
        $q['subjek'] = urldecode($this->input->get('q[subjek]', true));
        $q['usia'] = urldecode($this->input->get('q[usia]', true));
        $q['nama_kotakab'] = urldecode($this->input->get('q[nama_kotakab]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().$this->router->fetch_module();
            $config['first_url'] = base_url().$this->router->fetch_module();
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->keluhan->total_rows($q, ['konsultan' => null]);
        $keluhan = $this->keluhan->get_limit_data($config['per_page'], $start, $q, ['konsultan' => null]);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = [
            'title' => 'Keluhan',
            'keluhan' => $keluhan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        ];
        
        $this->layout->render('main', $data);
    }

    public function detail($id_keluhan)
    {
        $keluhan = $this->keluhan->get_by_id($id_keluhan);
        $keluhan_detail = $this->keluhan_detail->get_by_keluhan($id_keluhan);

        $jadwal = $this->jadwal->get_limit_data(1, 0, null, 'jadwal.keluhan = '.$id_keluhan);

        $data = [
            'title' => 'Detail Keluhan',
            'keluhan' => $keluhan,
            'keluhan_detail' => $keluhan_detail,
            'jadwal' => $jadwal,
            'load_css' => [
                'select2.min.css',
                'bootstrap-datetimepicker.min.css',
            ],
            'load_js' => [
                'select2.min.js',
                'bootstrap-datetimepicker.min.js',
            ],
            'load_script' => [
                '$(".datetimepicker").datetimepicker({
                    format : "YYYY-MM-DD HH:mm:ss"
                });
                $(".select2").select2();',
                $this->input->post('submit', true) ? '$("#myModal").modal("show")' : '',
            ],
        ];

        $this->layout->render('detail', $data);
    }

    public function buat_jadwal($id_keluhan)
    {
        if ($this->input->post('submit', true)) {
            $this->form_validation->set_rules('judul', 'judul', 'trim|required');
            $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
            $this->form_validation->set_rules('waktu', 'waktu', 'trim|required');
            $this->form_validation->set_rules('lokasi', 'lokasi', 'trim|required');

            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

            if ($this->form_validation->run()) {
                $data = [
                    'judul'         => $this->input->post('judul', true),
                    'deskripsi'     => $this->input->post('deskripsi', true),
                    'waktu'         => $this->input->post('waktu', true),
                    'lokasi'        => $this->input->post('lokasi', true),
                    'keluhan'       => $id_keluhan,
                    'created_at'    => date('Y-m-d H:m:s'),
                    'soft_delete'   => 0,
                ];

                $this->jadwal->insert($data);

                $this->session->set_flashdata('message', alert('success', 'Jadwal berhasil disimpan.'));
                redirect(site_url($this->router->fetch_module().'/keluhan/detail/'.$id_keluhan));
            } else {
                $this->detail($id_keluhan);
            }
        }
    }

    public function balas($id_keluhan)
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->detail($id_keluhan);
        } else {
            $this->keluhan_detail->insert([
                'keluhan'       => $id_keluhan,
                'pengirim'      => 2,
                'isi'           => nl2br($this->input->post('isi', true)),
                'created_at'    => date('Y-m-d H:i:s'),
                'soft_delete'   => 0,
            ]);

            $keluhan = $this->keluhan->get_by_id($id_keluhan);
            $this->notif->insert([
                'konsultan'     => $this->session->id_konsultan,
                'pasien'        => $keluhan->pasien,
                'penerima'      => 3,
                'isi'           => $this->session->nama_konsultan.' mengirimkan pesan.',
                'uri'           => 'keluhan/detail/'.$id_keluhan,
                'created_at'    => date('Y-m-d H:i:s'),
                'dibaca'        => 0,
            ]);

            $this->session->set_flashdata('message', alert('success', 'Pesan Sudah Terkirim'));
            redirect(site_url($this->router->fetch_module().'/keluhan/detail/'.$id_keluhan));
        }
    }

    public function tangani($id_keluhan)
    {
        $keluhan = $this->keluhan->get_by_id($id_keluhan);

        if ($keluhan->konsultan == null) {
            $this->keluhan->update($id_keluhan, ['konsultan' => $this->session->id_konsultan]);
            $this->konsultan->decrease_point($this->session->id_konsultan, 1);
            redirect(site_url($this->router->fetch_module().'/keluhan/detail/'.$id_keluhan));
        } else {
            redirect(site_url($this->router->fetch_module()));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('isi', 'isi pesan', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}
