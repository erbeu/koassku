<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class App extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

		$this->load->model('konsultan_model', 'konsultan');
		$this->load->model('keluhan_model', 'keluhan');

        if (!$this->konsultan->is_loggedin()) {
            redirect(site_url($this->router->fetch_module().'/app/login'));
        }
    }

    public function index()
    {
        $q['keluhan.created_at'] = urldecode($this->input->get('q[waktu]', true));
        $q['nama_pasien'] = urldecode($this->input->get('q[nama_pasien]', true));
        $q['subjek'] = urldecode($this->input->get('q[subjek]', true));
        $q['usia'] = urldecode($this->input->get('q[usia]', true));
        $q['nama_kotakab'] = urldecode($this->input->get('q[nama_kotakab]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().$this->router->fetch_module();
            $config['first_url'] = base_url().$this->router->fetch_module();
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->keluhan->total_rows($q);
        $keluhan = $this->keluhan->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = [
            'title' => 'Keluhan',
            'keluhan' => $keluhan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        ];
        
        $this->layout->render('beranda', $data);
    }

    public function login()
    {
        $this->layout->set_layout('login');

        $data = [
            'title' => 'Panel Login Coass'
        ];

        $this->layout->render('login', $data);
    }

    public function logout()
    {
        session_destroy();
        redirect(base_url($this->router->fetch_module().'/app/login'));
    }
}
