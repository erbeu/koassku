<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profil extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

		$this->load->model('konsultan_model', 'konsultan');
		$this->load->model('keluhan_model', 'keluhan');

        if (!$this->session->is_loggedin) {
            redirect(site_url($this->router->fetch_module().'/app/login'));
        }
    }

    public function index()
    {

        $q['keluhan.created_at'] = urldecode($this->input->get('q[waktu]', true));
        $q['nama_pasien'] = urldecode($this->input->get('q[nama_pasien]', true));
        $q['subjek'] = urldecode($this->input->get('q[subjek]', true));
        $q['usia'] = urldecode($this->input->get('q[usia]', true));
        $q['nama_kotakab'] = urldecode($this->input->get('q[nama_kotakab]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().$this->router->fetch_module();
            $config['first_url'] = base_url().$this->router->fetch_module();
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->keluhan->total_rows($q, ['konsultan' => $this->session->id_konsultan]);
        $pasien = $this->keluhan->get_limit_data($config['per_page'], $start, $q, ['konsultan' => $this->session->id_konsultan]);

        $this->keluhan->order = 'id_keluhan RAND()';
        $rekomendasi = $this->keluhan->get_limit_data($config['per_page'], $start, $q, ['konsultan' => null]);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $this->load->model('kotakab_model', 'kotakab');
        $konsultan = $this->konsultan->get_by_id_ori($this->session->id_konsultan);

        $data = [
            'title' => 'Profil Konsultan',
            'konsultan' => $konsultan,
            'pasien' => $pasien,
            'rekomendasi' => $rekomendasi,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'kotakab'       => $this->kotakab->get_all('array'),
            'jenis_kelamin' => $this->config->item('data_provider')['jenis_kelamin'],
            'gol_darah'     => $this->config->item('data_provider')['gol_darah'],
            'status_nikah'  => $this->config->item('data_provider')['status_nikah'],
            'agama'         => $this->config->item('data_provider')['agama'],
            'usia'          => $this->config->item('data_provider')['usia'],
        ];

        $this->layout->render('main', $data);
    }

    public function edit()
    {
        if ($this->input->post('submit', true)) {
            $this->_rules();

            if ($this->form_validation->run()) {
                $data = [
                    'email' => $this->input->post('email', true),
                    'nama_konsultan' => $this->input->post('nama_konsultan', true),
                    'no_mahasiswa' => $this->input->post('no_mahasiswa', true),
                    'no_ktp' => $this->input->post('no_ktp', true),
                    'no_hp' => $this->input->post('no_hp', true),
                    'alamat' => $this->input->post('alamat', true),
                    'kotakab' => $this->input->post('kotakab', true),
                    'tempat_lahir' => $this->input->post('tempat_lahir', true),
                    'tanggal_lahir' => $this->input->post('tanggal_lahir', true),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin', true),
                    'gol_darah' => $this->input->post('gol_darah', true),
                    'status_nikah' => $this->input->post('status_nikah', true),
                    'agama' => $this->input->post('agama', true),
                ];

                if (!empty($this->input->post('password', true))) {
                    $data['password'] = password_hash($this->input->post('password', true), PASSWORD_DEFAULT);
                }

                if (!empty($_FILES['foto']['name'])) {
                    
                    $data['foto'] = $config['file_name'];
                    $config['upload_path'] = './_uploads/konsultan/';
                    $config['file_name'] = $this->input->post('email', true).'.'.pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['max_size'] = 2048;
                    $config['max_width'] = 2048;
                    $config['max_height'] = 2048;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('foto')) {
                        $this->konsultan->update($this->session->id_konsultan, $data);
                        $this->session->set_flashdata('message', alert('success', 'Pendaftaran berhasil dilakukan, perlu ada sedikit verifikasi, dimohon untuk selalu mengecek email anda.'));
                        redirect(site_url($this->router->fetch_module().'/app/login'));
                    } else {
                        $this->session->set_flashdata('message', alert('danger', $this->upload->display_errors()));
                    }
                } else {
                    $this->konsultan->update($this->session->id_konsultan, $data);
                    $this->session->set_flashdata('message', alert('success', 'Perubahan berhasil disimpan.'));
                    redirect(site_url($this->router->fetch_module().'/profil/edit'));
                }
            }
        }

        $this->load->model('kotakab_model', 'kotakab');
        $this->load->model('gol_darah_model', 'gol_darah');
        $this->load->model('status_nikah_model', 'status_nikah');
        $this->load->model('agama_model', 'agama');

        $konsultan = $this->konsultan->get_by_id_ori($this->session->id_konsultan);

        $data = [
            'title' => 'Edit Profil',
            'list_kotakab' => $this->kotakab->get_all(),
            'list_gol_darah' => $this->gol_darah->get_all(),
            'list_status_nikah' => $this->status_nikah->get_all(),
            'list_agama' => $this->agama->get_all(),
            'konsultan' => $konsultan,
            'load_css' => [
                'select2.min.css',
                'bootstrap-datetimepicker.min.css',
            ],
            'load_js' => [
                'select2.min.js',
                'bootstrap-datetimepicker.min.js',
            ],
            'load_script' => [
                '$(".datetimepicker").datetimepicker({
                    format : "YYYY-MM-DD HH:mm:ss"
                });
                $(".select2").select2();',
            ],
        ];

        $this->layout->render('edit', $data);
    }

    public function _rules()
    {
        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|callback_email_check');
        $this->form_validation->set_rules('nama_konsultan', 'nama', 'trim|required');
        $this->form_validation->set_rules('no_mahasiswa', 'no mahasiswa', 'trim|required');
        $this->form_validation->set_rules('no_ktp', 'no ktp', 'trim|numeric|required');
        $this->form_validation->set_rules('no_hp', 'no hp', 'trim|numeric|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_rules('kotakab', 'kota/kab', 'trim|required');
        $this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
        $this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
        $this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
        $this->form_validation->set_rules('gol_darah', 'gol darah', 'trim|required');
        $this->form_validation->set_rules('status_nikah', 'status nikah', 'trim|required');
        $this->form_validation->set_rules('agama', 'agama', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function email_check($email)
    {
        if ($this->konsultan->search_email($email) != 1) {
            $this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
            return false;
        } else {
            return true;
        }
    }
}
