<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class App extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('konsultan_model', 'konsultan');
        $this->load->model('keluhan_model', 'keluhan');
        $this->load->model('kotakab_model', 'kotakab');
        $this->load->model('notif_model', 'notif');
    }

    public function index()
    {
        if (!$this->session->is_loggedin || $this->session->role != 'konsultan') {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }

        $q['keluhan.created_at'] = urldecode($this->input->get('q[waktu]', true));
        $q['nama_pasien'] = urldecode($this->input->get('q[nama_pasien]', true));
        $q['subjek'] = urldecode($this->input->get('q[subjek]', true));
        $q['usia'] = urldecode($this->input->get('q[usia]', true));
        $q['nama_kotakab'] = urldecode($this->input->get('q[nama_kotakab]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().$this->router->fetch_module();
            $config['first_url'] = base_url().$this->router->fetch_module();
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->keluhan->total_rows($q, ['konsultan' => null]);
        $keluhan = $this->keluhan->get_limit_data($config['per_page'], $start, $q, ['konsultan' => null]);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = [
            'title' => 'Dasbor',
            'keluhan' => $keluhan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        ];
        
        $this->layout->render('dasbor', $data);
    }

    public function login()
    {
        if ($this->session->is_loggedin && $this->session->role == 'konsultan') {
            redirect(site_url($this->router->fetch_module()));
        }

        if ($this->input->post('submit', true)) {
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $login = $this->konsultan->login($email, $password);

            if ($login == 1) {
                redirect(site_url($this->router->fetch_module()));
            } elseif ($login == 2) {
                $this->session->set_flashdata('message', alert('danger', 'Username / password salah'));
            } elseif ($login == 3) {
                $this->session->set_flashdata('message', alert('danger', 'Akun anda sedang ditangguhkan'));
            } else {
                $this->session->set_flashdata('message', alert('danger', 'Error'));
            }
        }

        $this->layout->set_layout('login');

        $data = [
            'title' => 'Login',
            'data' => [
                'email' => set_value('email'),
            ]
        ];

        $this->layout->render('login', $data);
    }

    public function daftar()
    {
        if ($this->session->is_loggedin && $this->session->role == 'konsultan') {
            redirect(site_url($this->router->fetch_module()));
        }

        if ($this->input->post('submit', true)) {
            $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|is_unique[konsultan.email]');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('nama_konsultan', 'nama', 'trim|required');
            $this->form_validation->set_rules('no_mahasiswa', 'no mahasiswa', 'trim|required');
            $this->form_validation->set_rules('no_ktp', 'no ktp', 'trim|numeric|required');
            $this->form_validation->set_rules('no_hp', 'no hp', 'trim|numeric|required');
            $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
            $this->form_validation->set_rules('kotakab', 'kota/kab', 'trim|required');
            $this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
            $this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
            $this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
            $this->form_validation->set_rules('gol_darah', 'gol darah', 'trim|required');
            $this->form_validation->set_rules('status_nikah', 'status nikah', 'trim|required');
            $this->form_validation->set_rules('agama', 'agama', 'trim|required');
            if (empty($_FILES['foto']['name'])) {
                $this->form_validation->set_rules('foto', 'foto', 'required');
            }

            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

            if ($this->form_validation->run()) {
                $config['upload_path'] = './_uploads/konsultan/';
                $config['file_name'] = rand(1,999).'_'.md5($this->input->post('email', true)).'.'.pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = 2048;
                $this->load->library('upload', $config);

                $data = [
                    'email'             => $this->input->post('email', true),
                    'password'          => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
                    'nama_konsultan'    => $this->input->post('nama_konsultan', true),
                    'poin'              => 15,
                    'no_mahasiswa'      => $this->input->post('no_mahasiswa', true),
                    'no_ktp'            => $this->input->post('no_ktp', true),
                    'no_hp'             => $this->input->post('no_hp', true),
                    'alamat'            => $this->input->post('alamat', true),
                    'kotakab'           => $this->input->post('kotakab', true),
                    'tempat_lahir'      => $this->input->post('tempat_lahir', true),
                    'tanggal_lahir'     => $this->input->post('tanggal_lahir', true),
                    'jenis_kelamin'     => $this->input->post('jenis_kelamin', true),
                    'gol_darah'         => $this->input->post('gol_darah', true),
                    'status_nikah'      => $this->input->post('status_nikah', true),
                    'agama'             => $this->input->post('agama', true),
                    'foto'              => $config['file_name'],
                    'created_at'        => date('Y-m-d H:m:s'),
                    'status'            => 0,
                    'diverifikasi_oleh' => null,
                    'soft_delete'       => 0,
                ];
                
                if ($this->upload->do_upload('foto')) {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $config['upload_path'].$config['file_name'];
                    $config['width'] = 250;
                    $config['height'] = 250;
                    $config['maintain_ratio'] = true;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $this->konsultan->insert($data);
                    $this->session->set_flashdata('message', alert('success', 'Pendaftaran berhasil dilakukan, silahkan cek email anda untuk petunjuk lebih lanjut.'));

                    $this->load->library('email');
                    $this->email->from($this->config->item('app')['email'], $this->config->item('app')['name']);
                    $this->email->to($data['email']);
                    $this->email->subject('Selamat Datang');

                    $email_message = $this->load->view('layouts/email/selamat_datang', ['nama_konsultan' => $data['nama_konsultan']], true);
                    $this->email->message($email_message);
                    $this->email->send();

                    redirect(site_url($this->router->fetch_module().'/app/login/'));
                } else {
                    $this->session->set_flashdata('message', alert('danger', $this->upload->display_errors()));
                }
            }
        }

        $this->layout->set_layout('daftar');

        $data = [
            'title' => 'Pendaftaran Konsultan',
            'load_css' => [
                'select2.min.css',
                'bootstrap-datetimepicker.min.css',
            ],
            'load_js' => [
                'select2.min.js',
                'bootstrap-datetimepicker.min.js',
            ],
            'load_script' => [
                '$(".datetimepicker").datetimepicker({
                    format : "YYYY-MM-DD HH:mm:ss"
                });
                $(".select2").select2();',
            ],
            'kotakab'       => $this->kotakab->get_all(),
            'jenis_kelamin' => $this->config->item('data_provider')['jenis_kelamin'],
            'gol_darah'     => $this->config->item('data_provider')['gol_darah'],
            'status_nikah'  => $this->config->item('data_provider')['status_nikah'],
            'agama'         => $this->config->item('data_provider')['agama'],
            'pendidikan'    => $this->config->item('data_provider')['pendidikan'],
        ];

        $this->layout->render('daftar', $data);
    }

    public function logout()
    {
        session_destroy();
        redirect(site_url($this->router->fetch_module().'/app/login/'));
    }
    
    public function baca_notif($id)
    {
        if (!$this->session->is_loggedin || $this->session->role != 'konsultan') {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }

        $notif = $this->notif->get_by_id($id);
        if ($notif->konsultan == $this->session->id_konsultan && $notif->penerima = 2) {
            $this->notif->update($id, ['dibaca' => 1]);
            redirect(site_url($this->router->fetch_module().'/'.$notif->uri));
        }

        redirect(site_url($this->router->fetch_module()));
    }
}
