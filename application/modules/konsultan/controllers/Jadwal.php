<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('konsultan_model', 'konsultan');
        $this->load->model('jadwal_model', 'jadwal');
        $this->load->model('keluhan_model', 'keluhan');
        $this->load->model('pasien_model', 'pasien');

        if (!$this->session->is_loggedin) {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }
    }

    public function index()
    {
        $q['waktu'] = urldecode($this->input->get('q[waktu]', true));
        $q['judul'] = urldecode($this->input->get('q[judul]', true));
        $q['lokasi'] = urldecode($this->input->get('q[lokasi]', true));
        $q['nama_pasien'] = urldecode($this->input->get('q[nama_pasien]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().$this->router->fetch_module();
            $config['first_url'] = base_url().$this->router->fetch_module();
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->jadwal->total_rows($q, ['keluhan.konsultan' => $this->session->id_konsultan]);
        $jadwal = $this->jadwal->get_limit_data($config['per_page'], $start, $q, ['keluhan.konsultan' => $this->session->id_konsultan]);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = [
            'title' => 'Jadwal',
            'jadwal' => $jadwal,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'load_css' => [
                'fullcalendar.min.css',
            ],
            'load_js' => [
                'fullcalendar.min.js',
            ],
        ];
        
        $this->layout->render('main', $data);
    }

    public function detail($id_jadwal)
    {
        $jadwal = $this->jadwal->get_by_id($id_jadwal);
        $keluhan = $this->keluhan->get_by_id($jadwal->keluhan);
        $konsultan = $this->konsultan->get_by_id($keluhan->konsultan);
        $pasien = $this->pasien->get_by_id($this->session->id_pasien);

        $data = [
            'title' => 'Detail Jadwal',
            'jadwal' => $jadwal,
            'keluhan' => $keluhan,
            'konsultan' => $konsultan,
            'pasien' => $pasien,
        ];

        $this->layout->render('detail', $data);
    }

    public function _rules()
    {
        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
        $this->form_validation->set_rules('waktu', 'waktu', 'trim|required');
        $this->form_validation->set_rules('lokasi', 'lokasi', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}
