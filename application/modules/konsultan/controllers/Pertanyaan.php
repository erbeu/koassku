<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pertanyaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pertanyaan_model', 'pertanyaan');
        $this->load->model('jawaban_model', 'jawaban');
        $this->layout->set_layout('admin');

        if (!$this->user->is_loggedin()) {
            redirect(site_url('admin/app/login'));
        }
        if ($this->session->role != 'admin') {
            redirect(site_url('admin'));
        }
    }

    public function index()
    {
        $q['id_pertanyaan'] = urldecode($this->input->get('q[id_pertanyaan]', true));
        $q['isi_pertanyaan'] = urldecode($this->input->get('q[isi_pertanyaan]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().'admin/pertanyaan?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().'admin/pertanyaan?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().'admin/pertanyaan';
            $config['first_url'] = base_url().'admin/pertanyaan';
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->pertanyaan->total_rows($q);
        $pertanyaan = $this->pertanyaan->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = [
            'title' => 'Pertanyaan',
            'pertanyaan' => $pertanyaan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        ];
        
        $this->layout->render_admin('list', $data);
    }

    public function view($id)
    {
        $pertanyaan = $this->pertanyaan->get_by_id($id);
        $jawaban = $this->jawaban->get_by_pertanyaan($id);

        if ($pertanyaan) {
            $data = [
                'title' => 'Detail Pertanyaan',
                'pertanyaan' => $pertanyaan,
                'jawaban' => $jawaban,
            ];

            $this->layout->render_admin('view', $data);
        } else {
            $this->session->set_flashdata('message', alert('danger', 'Data Tidak Ditemukan'));
            redirect(site_url('admin/pertanyaan'));
        }
    }

    public function create()
    {
        $data = [
            'title' => 'Buat Pertanyaan',
            'button' => 'Buat',
            'action' => site_url('admin/pertanyaan/create_action'),
            'id_pertanyaan' => set_value('id_pertanyaan'),
            'isi_pertanyaan' => set_value('isi_pertanyaan')
        ];

        for ($i = 1; $i <= 4; $i++) {
            $data['id_jawaban'][$i] = set_value('id_jawaban['.$i.']');
            $data['jawaban'][$i] = set_value('jawaban['.$i.']');
        }

        $this->layout->render_admin('form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data['pertanyaan'] = [
                'isi_pertanyaan' => $this->input->post('isi_pertanyaan', true),
            ];
            $pertanyaan = $this->pertanyaan->insert($data['pertanyaan']);

            foreach ($this->input->post('id_jawaban', true) as $key => $value) {
                $data['jawaban'][] = [
                    'id_pertanyaan' => $pertanyaan,
                    'isi_jawaban' => $this->input->post('jawaban['.$key.']', true)
                ];
            }
            $jawaban = $this->jawaban->insert_batch($data['jawaban']);

            $this->session->set_flashdata('message', alert('success', 'Data Berhasil Ditambah'));
            redirect(site_url('admin/pertanyaan/view/'.$pertanyaan));
        }
    }

    public function update($id)
    {
        $pertanyaan = $this->pertanyaan->get_by_id($id);
        $jawaban = $this->jawaban->get_by_pertanyaan($pertanyaan->id_pertanyaan);

        if ($pertanyaan) {
            $data = [
                'title' => 'Update Pertanyaan',
                'button' => 'Update',
                'action' => site_url('admin/pertanyaan/update_action'),
                'id_pertanyaan' => set_value('id_pertanyaan', $pertanyaan->id_pertanyaan),
                'isi_pertanyaan' => set_value('isi_pertanyaan', $pertanyaan->isi_pertanyaan),
            ];

            foreach ($jawaban as $key => $value) {
                $key += 1;
                $data['id_jawaban'][$key] = set_value('id_jawaban['.$key.']', $value->id_jawaban);
                $data['jawaban'][$key] = set_value('jawaban['.$key.']', $value->isi_jawaban);
            }

            $this->layout->render_admin('form', $data);
        } else {
            $this->session->set_flashdata('message', alert('danger', 'Data Tidak Ditemukan'));
            redirect(site_url('admin/pertanyaan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id_pertanyaan', true));
        } else {
            $data['pertanyaan'] = [
                'isi_pertanyaan' => $this->input->post('isi_pertanyaan', true),
            ];
            $this->pertanyaan->update($this->input->post('id_pertanyaan', true), $data['pertanyaan']);

            foreach ($this->input->post('id_jawaban', true) as $key => $value) {
                $data['jawaban'] = [
                    'isi_jawaban' => $this->input->post('jawaban['.$key.']', true)
                ];
                $this->jawaban->update($this->input->post('id_jawaban['.$key.']', true), $data['jawaban']);
            }

            $this->session->set_flashdata('message', alert('success', 'Data Berhasil Diupdate'));
            redirect(site_url('admin/pertanyaan'));
        }
    }

    public function delete($id)
    {
        $pertanyaan = $this->pertanyaan->get_by_id($id);

        if ($pertanyaan) {
            $this->pertanyaan->delete($id);
            $this->session->set_flashdata('message', alert('success', 'Data Berhasil Dihapus'));
            redirect(site_url('admin/pertanyaan'));
        } else {
            $this->session->set_flashdata('message', alert('danger', 'Data Tidak Ditemukan'));
            redirect(site_url('admin/pertanyaan'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('isi_pertanyaan', 'isi pertanyaan', 'trim|required');
        $this->form_validation->set_rules('id_pertanyaan', 'id pertanyaan', 'trim');

        $this->form_validation->set_rules('jawaban[1]', 'jawaban 1', 'trim|required');
        $this->form_validation->set_rules('jawaban[2]', 'jawaban 2', 'trim|required');
        $this->form_validation->set_rules('jawaban[3]', 'jawaban 3', 'trim|required');
        $this->form_validation->set_rules('jawaban[4]', 'jawaban 4', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}
