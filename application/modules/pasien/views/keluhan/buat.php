<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push('Keluhan', $this->router->fetch_module().'/keluhan/');
$this->breadcrumbs->push($title, $this->router->fetch_module().'/keluhan/buat/');
?>

<h3><i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <?= $title ?></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<?= $this->session->userdata('message') <> '' ? $this->session->userdata('message') : '' ?>

<div class="row">
    <div class="col-md-4">
        <?= $this->load->view('layouts/parts/panel-profil') ?>

        <a href="<?= site_url($this->router->fetch_module().'/keluhan/') ?>" class="btn btn-default btn-block"><i class="fa fa-chevron-left fa-fw" aria-hidden="true"></i> Kembali</a>
    </div>

    <div class="col-md-8">
        <h4>Keluhan Anda</h4>
        <hr>

        <form action="" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="subjek">Subjek <?= form_error('subjek') ?></label>
                        <input type="text" name="subjek" class="form-control" id="subjek" placeholder="Subjek" value="<?= set_value('subjek') ?>">
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsikan Keluhan Anda <?= form_error('bagian_sakit') ?></label>
                        <textarea name="deskripsi" id="deskripsi" rows="5" class="form-control" placeholder="Deskripsi"><?= set_value('deskripsi') ?></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin <?= form_error('jenis_kelamin') ?></label>
                        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control select2">
                            <option value="">- Pilih Jenis Kelamin -</option>
                            <?php foreach($jenis_kelamin as $key => $value) : ?>
                                <option value="<?= $key ?>" <?= set_value('jenis_kelamin') == $key ? 'selected' : '' ?>>
                                    <?= $value ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="usia">Usia <?= form_error('usia') ?></label>
                        <select name="usia" id="usia" class="form-control select2">
                            <option value="">- Pilih Usia -</option>
                            <?php foreach($usia as $key => $value) : ?>
                                <option value="<?= $key ?>" <?= set_value('usia') == $key ? 'selected' : '' ?>>
                                    <?= $value ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status_nikah">Status Nikah <?= form_error('status_nikah') ?></label>
                        <select name="status_nikah" id="status_nikah" class="form-control select2">
                            <option value="">- Status Nikah -</option>
                            <?php foreach($status_nikah as $key => $value) : ?>
                                <option value="<?= $key ?>" <?= set_value('status_nikah') == $key ? 'selected' : '' ?>>
                                    <?= $value ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="bagian_sakit">Bagian yang Terasa Sakit <?= form_error('bagian_sakit') ?></label>
                        <input type="text" name="bagian_sakit" class="form-control" id="bagian_sakit" placeholder="Bagian Sakit" value="<?= set_value('bagian_sakit') ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="lama_sakit">Lama Sakit <?= form_error('lama_sakit') ?></label>
                        <input type="text" name="lama_sakit" class="form-control" id="lama_sakit" placeholder="Lama Sakit" value="<?= set_value('lama_sakit') ?>">
                    </div>
                    <div class="form-group">
                        <label for="obat_kumur">Apakah Menggunakan Obat Kumur <?= form_error('obat_kumur') ?></label>
                        <select name="obat_kumur" id="obat_kumur" class="form-control select2">
                            <option value="">- Obat Kumur -</option>
                            <?php foreach($ya_tidak as $key => $value) : ?>
                                <option value="<?= $key ?>" <?= set_value('obat_kumur') == $key ? 'selected' : '' ?>>
                                    <?= $value ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="jenis_bulu_sikat">Jenis Bulu Sikat yang Digunakan <?= form_error('jenis_bulu_sikat') ?></label>
                        <select name="jenis_bulu_sikat" id="jenis_bulu_sikat" class="form-control select2">
                            <option value="">- Jenis Bulu Sikat -</option>
                            <?php foreach($jenis_bulu_sikat as $key => $value) : ?>
                                <option value="<?= $key ?>" <?= set_value('jenis_bulu_sikat') == $key ? 'selected' : '' ?>>
                                    <?= $value ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="foto_gigi">Foto Gigi (Jika Diperlukan) <?= form_error('foto_gigi') ?></label>
                        <input type="file" name="foto_gigi" class="form-control" id="foto_gigi">
                        <small class="help-text">Maksimum ukuran foto 2MB.</small>
                    </div>
                    <hr>
                    <button type="submit" name="submit" value="1" class="btn btn-primary pull-right">Buat Keluhan <i class="fa fa-chevron-right fa-fw" aria-hidden="true"></i></button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </form>
    </div>
</div>



