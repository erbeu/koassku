<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push('Keluhan', $this->router->fetch_module().'/keluhan/');
$this->breadcrumbs->push($title, $this->router->fetch_module().'/keluhan/detail/'.$keluhan->id_keluhan);
?>

<h3><i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <?= $title ?></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<?= $this->session->flashdata('message') != null ? $this->session->flashdata('message') : '' ?>

<div class="row">
    <div class="col-md-4">
        <?= $this->load->view('layouts/parts/panel-profil') ?>

        <div class="panel panel-default">
            <table class="table table-bordered">
                <tr>
                    <td>Konsultan</td>
                    <td>
                        <?= $keluhan->nama_konsultan ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%">Subjek</td>
                    <td>
                        <?= $keluhan->subjek ?>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>
                        <?= $this->config->item('data_provider')['jenis_kelamin'][$keluhan->jenis_kelamin] ?>
                    </td>
                </tr>
                <tr>
                    <td>Usia</td>
                    <td>
                        <?= $this->config->item('data_provider')['usia'][$keluhan->usia] ?>
                    </td>
                </tr>
                <tr>
                    <td>Bagian Sakit</td>
                    <td>
                        <?= $keluhan->bagian_sakit ?>
                    </td>
                </tr>
                <tr>
                    <td>Lama Sakit</td>
                    <td>
                        <?= $keluhan->lama_sakit ?>
                    </td>
                </tr>
                <tr>
                    <td>Menggunakan Obat Kumur</td>
                    <td>
                        <?= $this->config->item('data_provider')['ya_tidak'][$keluhan->obat_kumur] ?>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Bulu Sikat</td>
                    <td>
                        <?= $this->config->item('data_provider')['jenis_bulu_sikat'][$keluhan->jenis_bulu_sikat] ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p>Foto Gigi</p>
                        <?php if ($keluhan->foto_gigi == null) : ?>
                            <p>Tidak ada foto</p>
                        <?php else : ?>
                            <img src="<?= base_url('_uploads/gigi/'.$keluhan->foto_gigi) ?>" alt="Foto Gigi" class="img-responsive">
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>

        <a href="<?= site_url($this->router->fetch_module().'/keluhan/') ?>" class="btn btn-default btn-block"><i class="fa fa-chevron-left fa-fw" aria-hidden="true"></i> Kembali</a>
    </div>

    <div class="col-md-8">
        <div class="panel panel-default message-area">
            <div class="row">
                <div class="col-md-8 col-md-offset-4">
                    <div class="well">
                        <b>Anda</b>
                        <br>
                        <?= $keluhan->deskripsi ?>
                        <br>
                        <small class="message-time label label-info"><?= $keluhan->created_at ?></small>
                    </div>
                </div>
            </div>

            <?php foreach ($keluhan_detail as $data) : ?>
                <div class="row">
                    <div class="col-md-8 <?= $data->pengirim == 3 ? 'col-md-offset-4' : '' ?>">
                        <div class="well">
                            <b><?= $data->pengirim == 3 ? 'Anda' : $keluhan->nama_konsultan ?></b>
                            <br>
                            <?= $data->isi ?>
                            <br>
                            <small class="message-time label label-info"><?= $data->created_at ?></small>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <hr>

            <?php if ($keluhan->konsultan) : ?>
                <form action="<?= site_url($this->router->fetch_module().'/keluhan/balas/'.$keluhan->id_keluhan) ?>" method="post">
                    <div class="form-group">
                        <label for="isi"><?= form_error('isi') ?></label>
                        <textarea name="isi" id="isi" rows="5" placeholder="Tulis pesan anda disini..."><?= set_value('isi') ?></textarea>
                    </div>

                    <?php if (count($jadwal) != 0) : ?>
                        <a href="<?= site_url($this->router->fetch_module().'/jadwal/detail/'.$jadwal[0]->id_jadwal) ?>" class="btn btn-info pull-left"><span class="fa fa-calendar fa-fw"></span> Lihat Jadwal</a>
                    <?php endif; ?>

                    <button type="submit" name="submit" class="btn btn-primary pull-right" value="simpan">Kirim Pesan <i class="fa fa-chevron-right fa-fw" aria-hidden="true"></i></button>
                    <div class="clearfix"></div>
                </form>
            <?php else : ?>
                <div class="alert alert-info">Menunggu konsultan yang siap membantu anda.</div>
            <?php endif; ?>
        </div>
    </div>
</div>
