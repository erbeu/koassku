<?php
$this->load->model('pasien_model', 'pasien');
$pasien = $this->pasien->get_by_id($this->session->id_pasien);
?>

<div class="panel panel-default panel-profil">
    <div class="panel-body">
        <img src="<?= base_url('_uploads/pasien/'.$pasien->foto) ?>" alt="Foto" class="img-responsive img-center img-small img-thumbnail">
        <div class="text-center">
            <h4><?= $pasien->nama_pasien ?></h4>
            <small><?= $this->config->item('data_provider')['jenis_kelamin'][$pasien->jenis_kelamin] ?></small>
            <span class="label label-primary label-role">PASIEN</span>
        </div>
    </div>
</div>