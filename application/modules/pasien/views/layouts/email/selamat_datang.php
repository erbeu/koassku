<html>
<head>
    <title>Selamat Datang</title>
</head>
<body>
    <table cellpadding="0" cellspacing="0" style="max-width: 500px; width: 100%; margin-left: auto; margin-right: auto; font-family: Segoe UI; border: 1px solid #aaaaaa;">
        <tr>
            <td style="background-color: #2c3e50; color: #ffffff; padding: 20px; text-align: center; letter-spacing: -2px;">
                <h1>KOASSKU</h1>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px;">
                <p style="text-align: justify;">
                    Hai, <?= $nama_pasien ?><br>
                    Selamat, anda telah terdaftar di Koassku. Untuk memulai keluhan, silahkan kunjungi Koassku menggunakan email dan password yang sudah didaftarkan.
                </p>
                <a href="<?= site_url($this->router->fetch_module().'/app/login/') ?>" style="display: block; text-align: center; vertical-align: middle; cursor: pointer; padding: 10px 15px; font-size: 15px; line-height: 1.42857143; border-radius: 4px; color: #ffffff; background-color: #2c3e50; border-color: #2c3e50; border-width: 2px;">Login</a>
            </td>
        </tr>
        <tr>
            <td style="background-color: #f5f5f5; color: #aaaaaa; padding: 10px; text-align: center;">Pesan ini dikirim secara otomatis oleh sistem.</td>
        </tr>
    </table>
</body>
</html>
