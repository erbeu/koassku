<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $data['title'].' | '.$this->config->item('app')['name'] ?>
    </title>
    <link rel="stylesheet" href="<?= base_url('_assets/css/bootstrap-flatly.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('_assets/css/bootstrap-datetimepicker.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('_assets/css/select2.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('_assets/css/pasien.css') ?>">
</head>

<body class="pasien daftar">
    <div class="container header">
        <img src="<?= base_url('_assets/img/logokoasskubiru.png') ?>" alt="" class="img-responsive">
    </div>

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->load->view($view, $data); ?>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="container">
            <p class="text-muted">Copyright &copy; 2017. <?= $this->config->item('app')['name'] ?></p>
        </div>
    </div>

    <script src="<?= base_url('_assets/js/jquery-3.1.1.min.js') ?>"></script>
    <script src="<?= base_url('_assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('_assets/js/moment.js') ?>"></script>
    <script src="<?= base_url('_assets/js/bootstrap-datetimepicker.min.js') ?>"></script>
    <script src="<?= base_url('_assets/js/select2.min.js') ?>"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            $('.datetimepicker').datetimepicker({
                format : 'YYYY-MM-DD'
            });
        });
    </script>
</body>

</html>
