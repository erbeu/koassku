<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $data['title'].' | '.$this->config->item('app')['name'] ?>
    </title>
    <link rel="stylesheet" href="<?= base_url('_assets/css/bootstrap-flatly.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('_assets/css/pasien.css') ?>">
</head>

<body class="login">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7 login-bg">
            </div>
            <div class="col-md-5 box-login">
                <div class="box-login-inner">
                    <div class="header">
                        <img src="<?= base_url('_assets/img/logokoasskubiru.png') ?>" alt="" class="img-responsive">
                    </div>
                    <?php $this->load->view($view, $data); ?>
                </div>
            </div>
        </div>
    </div>
                

    <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-5">
                    <p class="text-muted">Copyright &copy; 2017. <?= $this->config->item('app')['name'] ?></p>
                </div>
            </div>
        </div>
    </div>

    <script src="<?= base_url('_assets/js/jquery-3.1.1.min.js') ?>"></script>
    <script src="<?= base_url('_assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('_assets/js/parallax.min.js') ?>"></script>
    <script src="<?= base_url('_assets/js/scrollreveal.min.js') ?>"></script>
    <script>
        $(document).ready(function() {});

    </script>
</body>

</html>
