<?php
$this->load->model('notif_model', 'notif');

$data['unread_notif'] = $this->notif->get_unread();
$data['notif_count'] = count($data['unread_notif']);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $data['title'].' | '.$this->config->item('app')['name'] ?>
    </title>
    <link rel="stylesheet" href="<?= base_url('_assets/css/bootstrap-flatly.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('_assets/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('_assets/css/pasien.css') ?>">
    <?php
    if (isset($data['load_css'])) {
        foreach ($data['load_css'] as $value) {
            echo '<link rel="stylesheet" href="'.base_url('_assets/css/'.$value).'">';
        }
    }
    ?>
</head>

<body class="pasien">
    <div class="header">
        <div class="container">
            <a href="<?= site_url() ?>"><img src="<?= base_url('_assets/img/logokoasskubiru.png') ?>" alt="Logo" class="logo"></a>
        </div>
    </div>

    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?= activate_menu('app') ?>">
                        <a href="<?= site_url($this->router->fetch_module()) ?>"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Dasbor</a>
                    </li>
                    <li class="<?= activate_menu('keluhan') ?>">
                        <a href="<?= site_url($this->router->fetch_module().'/keluhan/') ?>"><i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> Keluhan</a>
                    </li>
                    <li class="<?= activate_menu('jadwal') ?>">
                        <a href="<?= site_url($this->router->fetch_module().'/jadwal/') ?>"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i> Jadwal</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                           <span class="badge"><i class="fa fa-bell fa-fw" aria-hidden="true"></i> <?= $data['notif_count'] ?></span>
                       </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <?= $data['notif_count'] == 0 ? '<li><a>Tidak ada notifikasi</a></li>' : '' ?>

                            <?php foreach ($data['unread_notif'] as $row) : ?>
                                <li>
                                    <a href="<?= site_url($this->router->fetch_module().'/app/baca_notif/'.$row->id_notif) ?>">
                                        <?= $row->isi ?><br>
                                        <small class="momentjs label label-info"><?= $row->created_at ?></small>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <li class="<?= activate_menu('profil') ?>">
                        <a href="<?= site_url($this->router->fetch_module().'/profil/') ?>"><i class="fa fa-user fa-fw" aria-hidden="true"></i> Profil</a>
                    </li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-navicon fa-fw" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?= site_url($this->router->fetch_module().'/profil/edit') ?>">Edit Profil</a></li>
                            <li><a href="<?= site_url($this->router->fetch_module().'/app/logout') ?>">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <?php $this->load->view($view, $data); ?>
    </div>

    <div class="footer">
        <div class="container">
            <p class="text-muted">Copyright &copy; 2017. <?= $this->config->item('app')['name'] ?></p>
        </div>
    </div>

    <script src="<?= base_url('_assets/js/jquery-3.1.1.min.js') ?>"></script>
    <script src="<?= base_url('_assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('_assets/js/moment.js') ?>"></script>
    <script src="<?= base_url('_assets/js/parallax.min.js') ?>"></script>
    <?php
    if (isset($data['load_js'])) {
        foreach ($data['load_js'] as $value) {
            echo '<script src="'.base_url('_assets/js/'.$value).'"></script>';
        }
    }
    ?>
    <script>
        $(document).ready(function() {
            $('.parallax-window').parallax({
                imageSrc: '<?= base_url('_assets/img/coass-jumbotron.jpg') ?>'
            });

            moment.locale('id');
            $(".momentjs").each(function () {
                $(this).html(moment($(this).html(), "YYYY-MM-DD HH:mm:ss").fromNow());
            });

            <?php
            if (isset($data['load_script'])) {
                foreach ($data['load_script'] as $value) {
                    echo $value;
                }
            }
            ?>
        });
    </script>
</body>

</html>