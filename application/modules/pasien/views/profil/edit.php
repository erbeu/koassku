<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push('Profil', $this->router->fetch_module().'/profil/');
$this->breadcrumbs->push($title, $this->router->fetch_module().'/profil/edit/');
?>

<h3><i class="fa fa-user fa-fw" aria-hidden="true"></i> <?= $title ?></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<?= $this->session->flashdata('message') != null ? $this->session->flashdata('message') : '' ?>

<form action="" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="email">Email <?= form_error('email') ?></label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="<?= set_value('email', $pasien->email) ?>">
            </div>
            <div class="form-group">
                <label for="password">Password <?= form_error('password') ?></label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="">
                <p class="help-text">Kosongkan jika tidak ingin mengganti password.</p>
            </div>
            <div class="form-group">
                <label for="nama">Nama Lengkap <?= form_error('nama_pasien') ?></label>
                <input type="text" name="nama_pasien" class="form-control" id="nama" placeholder="Nama Lengkap" value="<?= set_value('nama_pasien', $pasien->nama_pasien) ?>">
            </div>
            <div class="form-group">
                <label for="no_hp">No Handphone <?= form_error('no_hp') ?></label>
                <input type="text" name="no_hp" class="form-control" id="no_hp" placeholder="No Handphone" value="<?= set_value('no_hp', $pasien->no_hp) ?>">
            </div>
            <div class="form-group">
                <label for="alamat">Alamat <?= form_error('alamat') ?></label>
                <textarea name="alamat" class="form-control" id="alamat" rows="3" placeholder="Alamat"><?= set_value('alamat', $pasien->alamat) ?></textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="kotakab">Kota/Kab <?= form_error('kotakab') ?></label>
                <select name="kotakab" id="kotakab" class="form-control select2">
                    <option value="">- Pilih Kota/Kab -</option>
                    <?php foreach($kotakab as $value) : ?>
                        <option value="<?= $value->id_kotakab ?>" <?= set_value('kotakab', $pasien->kotakab) == $value->id_kotakab ? 'selected' : '' ?>>
                            <?= $value->nama_kotakab ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="tempat_lahir">Tempat, Tanggal Lahir <?= form_error('tempat_lahir') ?> <?= form_error('tanggal_lahir') ?></label>
                <div class="row">
                    <div class="col-md-7">
                        <select name="tempat_lahir" id="tempat_lahir" class="form-control select2">
                            <option value="">- Pilih Tempat Lahir -</option>
                            <?php foreach($kotakab as $value) : ?>
                                <option value="<?= $value->id_kotakab ?>" <?= set_value('tempat_lahir', $pasien->tempat_lahir) == $value->id_kotakab ? 'selected' : '' ?>>
                                    <?= $value->nama_kotakab ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="tanggal_lahir" class="form-control datetimepicker" id="tanggal_lahir" placeholder="Tanggal Lahir" value="<?= set_value('tanggal_lahir', $pasien->tanggal_lahir) ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin <?= form_error('jenis_kelamin') ?></label>
                <select name="jenis_kelamin" id="jenis_kelamin" class="form-control select2">
                    <option value="">- Pilih Jenis Kelamin -</option>
                    <?php foreach($jenis_kelamin as $key => $value) : ?>
                        <option value="<?= $key ?>" <?= set_value('jenis_kelamin', $pasien->jenis_kelamin) == $key ? 'selected' : '' ?>>
                            <?= $value ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="gol_darah">Gol Darah <?= form_error('gol_darah') ?></label>
                <select name="gol_darah" id="gol_darah" class="form-control select2">
                    <option value="">- Pilih Gol Darah -</option>
                    <?php foreach($gol_darah as $key => $value) : ?>
                        <option value="<?= $key ?>" <?= set_value('gol_darah', $pasien->gol_darah) == $key ? 'selected' : '' ?>>
                            <?= $value ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="status_nikah">Status Nikah <?= form_error('status_nikah') ?></label>
                <select name="status_nikah" id="status_nikah" class="form-control select2">
                    <option value="">- Pilih Status Nikah -</option>
                    <?php foreach($status_nikah as $key => $value) : ?>
                        <option value="<?= $key ?>" <?= set_value('status_nikah', $pasien->status_nikah) == $key ? 'selected' : '' ?>>
                            <?= $value ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="agama">Agama <?= form_error('agama') ?></label>
                <select name="agama" id="agama" class="form-control select2">
                    <option value="">- Pilih Agama -</option>
                    <?php foreach($agama as $key => $value) : ?>
                        <option value="<?= $key ?>" <?= set_value('agama', $pasien->agama) == $key ? 'selected' : '' ?>>
                            <?= $value ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="pekerjaan">Pekerjaan <?= form_error('pekerjaan') ?></label>
                <input type="text" name="pekerjaan" class="form-control" id="pekerjaan" placeholder="Pekerjaan" value="<?= set_value('pekerjaan', $pasien->pekerjaan) ?>">
            </div>
            <div class="form-group">
                <label for="pendidikan">Pendidikan <?= form_error('pendidikan') ?></label>
                <select name="pendidikan" id="pendidikan" class="form-control select2">
                    <option value="">- Pilih Pendidikan -</option>
                    <?php foreach($pendidikan as $key => $value) : ?>
                        <option value="<?= $key ?>" <?= set_value('pendidikan', $pasien->pendidikan) == $key ? 'selected' : '' ?>>
                            <?= $value ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="alergi">Alergi Terhadap <?= form_error('alergi') ?></label>
                <input type="text" name="alergi" class="form-control" id="alergi" placeholder="Alergi" value="<?= set_value('alergi', $pasien->alergi) ?>">
            </div>
            <div class="form-group">
                <label for="foto">Foto <?= form_error('foto') ?></label>
                <input type="file" name="foto" class="form-control" id="foto">
                <input type="hidden" name="nama_foto" value="<?= explode('.', set_value('foto', $pasien->foto))[0] ?>">
                <small class="help-text">Maksimum ukuran foto 1MB.</small>
            </div>
        </div>
    </div>
    <a href="<?= site_url($this->router->fetch_module().'/profil/') ?>" class="btn btn-success pull-left"><span class="fa fa-chevron-left fa-fw"></span> Kembali</a>
    <button type="submit" name="submit" class="btn btn-primary pull-right" value="1"><i class="fa fa-check fa-fw"></i> Simpan</button>
    <div class="clearfix"></div>
</form>
