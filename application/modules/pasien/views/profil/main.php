<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push($title, $this->router->fetch_module().'/profil/');
?>

<h3><i class="fa fa-user fa-fw" aria-hidden="true"></i> <?= $title ?></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<?= $this->session->flashdata('message') != null ? $this->session->flashdata('message') : '' ?>

<div class="row">
    <div class="col-md-4">
        <?= $this->load->view('layouts/parts/panel-profil') ?>
    </div>

    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <table class="table">
                    <tr>
                        <td width="35%">Nama Lengkap</td>
                        <td>: <?= $pasien->nama_pasien ?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>: <?= $pasien->email ?></td>
                    </tr>
                    <tr>
                        <td>No Handphone</td>
                        <td>: <?= $pasien->no_hp ?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>: <?= $pasien->alamat ?></td>
                    </tr>
                    <tr>
                        <td>Kota/Kab</td>
                        <td>: <?= $kotakab[array_search($pasien->kotakab, array_column($kotakab, 'id_kotakab'))]['nama_kotakab'] ?></td>
                    </tr>
                    <tr>
                        <td>Tempat, Tanggal Lahir</td>
                        <td>: <?= $kotakab[array_search($pasien->tempat_lahir, array_column($kotakab, 'id_kotakab'))]['nama_kotakab'].', '.$pasien->tanggal_lahir ?></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>: <?= $this->config->item('data_provider')['jenis_kelamin'][$pasien->jenis_kelamin] ?></td>
                    </tr>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table">
                    <tr>
                        <td width="35%">Gol Darah</td>
                        <td>: <?= $this->config->item('data_provider')['gol_darah'][$pasien->gol_darah] ?></td>
                    </tr>
                    <tr>
                        <td>Status Nikah</td>
                        <td>: <?= $this->config->item('data_provider')['status_nikah'][$pasien->status_nikah] ?></td>
                    </tr>
                    <tr>
                        <td>Agama</td>
                        <td>: <?= $this->config->item('data_provider')['agama'][$pasien->agama] ?></td>
                    </tr>
                    <tr>
                        <td>Pekerjaan</td>
                        <td>: <?= $pasien->pekerjaan ?></td>
                    </tr>
                    <tr>
                        <td>Pendidikan</td>
                        <td>: <?= $this->config->item('data_provider')['pendidikan'][$pasien->pendidikan] ?></td>
                    </tr>
                    <tr>
                        <td>Alergi</td>
                        <td>: <?= $pasien->alergi ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>