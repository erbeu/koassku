<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push('Jadwal', $this->router->fetch_module().'/jadwal/');
$this->breadcrumbs->push($title, $this->router->fetch_module().'/jadwal/detail/'.$jadwal->id_jadwal);
?>

<h3><i class="fa fa-calendar fa-fw" aria-hidden="true"></i> <?= $title ?></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<?= $this->session->flashdata('message') != null ? $this->session->flashdata('message') : '' ?>

<div class="row">
    <div class="col-md-4">
        <?= $this->load->view('layouts/parts/panel-profil') ?>

        <div class="panel panel-default">
            <table class="table table-bordered">
                <tr>
                    <td width="50%">Subjek</td>
                    <td>
                        <?= $keluhan->subjek ?>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>
                        <?= $this->config->item('data_provider')['jenis_kelamin'][$keluhan->jenis_kelamin] ?>
                    </td>
                </tr>
                <tr>
                    <td>Usia</td>
                    <td>
                        <?= $this->config->item('data_provider')['usia'][$keluhan->usia] ?>
                    </td>
                </tr>
                <tr>
                    <td>Bagian Sakit</td>
                    <td>
                        <?= $keluhan->bagian_sakit ?>
                    </td>
                </tr>
                <tr>
                    <td>Lama Sakit</td>
                    <td>
                        <?= $keluhan->lama_sakit ?>
                    </td>
                </tr>
                <tr>
                    <td>Menggunakan Obat Kumur</td>
                    <td>
                        <?= $this->config->item('data_provider')['ya_tidak'][$keluhan->obat_kumur] ?>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Bulu Sikat</td>
                    <td>
                        <?= $this->config->item('data_provider')['jenis_bulu_sikat'][$keluhan->jenis_bulu_sikat] ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p>Foto Gigi</p>
                        <?php if ($keluhan->foto_gigi == null) : ?>
                            <p>Tidak ada foto</p>
                        <?php else : ?>
                            <img src="<?= base_url('_uploads/gigi/'.$keluhan->foto_gigi) ?>" alt="Foto Gigi" class="img-responsive">
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>

        <a href="<?= site_url($this->router->fetch_module().'/jadwal/') ?>" class="btn btn-default btn-block"><i class="fa fa-chevron-left fa-fw" aria-hidden="true"></i> Kembali</a>
    </div>

    <div class="col-md-4">
        <img src="<?= base_url('_assets/img/coass-jumbotron.jpg') ?>" class="img-responsive">
        <table class="table table-bordered">
            <tr>
                <td width="30%">Judul</td>
                <td width="70%">
                    <?= $jadwal->judul ?>
                </td>
            </tr>
            <tr>
                <td>Deskripsi</td>
                <td>
                    <?= $jadwal->deskripsi ?>
                </td>
            </tr>
            <tr>
                <td>Waktu</td>
                <td>
                    <?= $jadwal->waktu ?>
                </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td>
                    <?= $jadwal->lokasi ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="col-md-4">
        <div class="panel panel-default panel-profil">
            <div class="panel-body">
                <img src="<?= base_url('_uploads/konsultan/'.$konsultan->foto) ?>" alt="Foto" class="img-responsive img-center img-small img-thumbnail">
                <div class="text-center">
                    <h4><?= $konsultan->nama_konsultan ?></h4>
                    <small><?= $this->config->item('data_provider')['jenis_kelamin'][$konsultan->jenis_kelamin] ?></small>
                    <span class="label label-primary label-role">KONSULTAN</span>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <table class="table table-bordered">
                <tr>
                    <td width="50%">No HP</td>
                    <td>
                        <?= $konsultan->no_hp ?>
                    </td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>
                        <?= $this->config->item('data_provider')['jenis_kelamin'][$konsultan->jenis_kelamin] ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
