<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push($title, $this->router->fetch_module().'/jadwal/');
?>

<h3><i class="fa fa-calendar fa-fw" aria-hidden="true"></i> <?= $title ?></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<?= $this->session->flashdata('message') != null ? $this->session->flashdata('message') : '' ?>

<div class="row">
    <div class="col-md-4">
        <?= $this->load->view('layouts/parts/panel-profil') ?>
    </div>

    <div class="col-md-8">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Waktu</th>
                        <th>Judul</th>
                        <th>Nama Konsultan</th>
                        <th>Aksi</th>
                    </tr>
                    <tr>
                        <form action="" method="get">
                            <td></td>
                            <td>
                                <input type="text" class="form-control input-sm" name="q[waktu]" placeholder="Cari Waktu" value="<?= $q['waktu']; ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" name="q[judul]" placeholder="Cari Judul" value="<?= $q['judul']; ?>">
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" name="q[nama_konsultan]" placeholder="Cari Nama Konsultan" value="<?= $q['nama_konsultan']; ?>">
                            </td>
                            <td class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search fa-fw" aria-hidden="true"></i></button>
                            </td>
                        </form>
                    </tr>
                </thead>
                <tbody>
                    <?= (count($jadwal) == 0) ? '<td colspan="7">Tidak ada data.</td>' : '' ?>
                    <?php foreach ($jadwal as $data) : ?>
                        <tr>
                            <td>
                                <?= ++$start ?>
                            </td>
                            <td>
                                <?= $data->waktu ?>
                            </td>
                            <td>
                                <?= $data->judul ?>
                            </td>
                            <td>
                                <?= $data->nama_konsultan ?>
                            </td>
                            <td class="text-center">
                                <div class="btn-group btn-group-sm" role="group">
                                    <?php
                                    echo anchor(
                                        site_url($this->router->fetch_module().'/jadwal/detail/'.$data->id_jadwal),
                                        '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> Detail',
                                        ['class' => 'btn btn-success']
                                    );
                                    ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-xs-6">
                Total Record :
                <?= $total_rows ?>
            </div>
            <div class="col-xs-6 text-right">
                <?= $pagination ?>
            </div>
        </div>
    </div>
</div>