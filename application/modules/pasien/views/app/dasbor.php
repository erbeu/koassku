<?php
$this->breadcrumbs->push($this->config->item('app')['name'], '/');
$this->breadcrumbs->push($title, $this->router->fetch_module());
?>

<h3><i class="fa fa-home fa-fw" aria-hidden="true"></i> <?= $title ?> <small>Ikhtisar Aktivitas</small></h3>
<hr>
<?= $this->breadcrumbs->show(); ?>

<div class="row">
    <div class="col-md-4">
        <?= $this->load->view('layouts/parts/panel-profil') ?>
    </div>

    <div class="col-md-8">
        <h4>Keluhan Anda <a href="<?= site_url($this->router->fetch_module().'/keluhan/buat/') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus fa-fw" aria-hidden="true"></i> Keluhan Baru</a></h4>
        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Waktu</th>
                        <th>Subjek</th>
                        <th>Konsultan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?= (count($keluhan) == 0) ? '<td colspan="7">Tidak ada data.</td>' : '' ?>
                    <?php foreach ($keluhan as $data) : ?>
                        <tr>
                            <td>
                                <?= ++$start ?>
                            </td>
                            <td>
                                <?= $data->created_at ?>
                            </td>
                            <td>
                                <?= $data->subjek ?>
                            </td>
                            <td>
                                <?= $data->nama_konsultan ?>
                            </td>
                            <td class="text-center">
                                <div class="btn-group btn-group-sm" role="group">
                                    <?php
                                    echo anchor(
                                        site_url($this->router->fetch_module().'/keluhan/detail/'.$data->id_keluhan),
                                        '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> Detail',
                                        ['class' => 'btn btn-success']
                                    );
                                    ?>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-xs-6">
                Total Record :
                <?= $total_rows ?>
            </div>
            <div class="col-xs-6 text-right">
                <?= $pagination ?>
            </div>
        </div>
    </div>
</div>



