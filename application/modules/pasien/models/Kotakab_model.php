<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Kotakab_model extends CI_Model
{
    public $table = 'kotakab';
    public $id = 'id_kotakab';
    public $order = 'ASC';

    public function __construct()
    {
        parent::__construct();
    }

    // get all
    public function get_all($type = 'object')
    {
        $this->db->order_by($this->id, $this->order);

        if ($type == 'object') {
            return $this->db->get($this->table)->result();
        } elseif ($type == 'array') {
            return $this->db->get($this->table)->result_array();
        }
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }
}
