<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Notif_model extends CI_Model
{
    public $table = 'notif';
    public $id = 'id_notif';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // get by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    // get unread
    public function get_unread()
    {
        $this->db->where(['pasien' => $this->session->id_pasien, 'penerima' => 3, 'dibaca' => 0]);
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }
}
