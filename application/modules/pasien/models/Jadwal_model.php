<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jadwal_model extends CI_Model
{
    public $table = 'jadwal';
    public $id = 'id_jadwal';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    // get total rows
    public function total_rows($q = null, $w = null)
    {
        $this->db->join('keluhan', 'keluhan.id_keluhan = jadwal.keluhan', 'left');
        $this->db->join('konsultan', 'konsultan.id_konsultan = keluhan.konsultan', 'left');
        $this->db->join('pasien', 'pasien.id_pasien = keluhan.pasien', 'left');
        $this->db->order_by($this->id, $this->order);
        $this->db->from($this->table);

        if ($q != null) {
            $this->db->like($q);
        }

        if ($w != null) {
            $this->db->where($w);
        }

        return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = null, $w = null)
    {
        $this->db->join('keluhan', 'keluhan.id_keluhan = jadwal.keluhan', 'left');
        $this->db->join('konsultan', 'konsultan.id_konsultan = keluhan.konsultan', 'left');
        $this->db->join('pasien', 'pasien.id_pasien = keluhan.pasien', 'left');
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($limit, $start);

        if ($q != null) {
            $this->db->like($q);
        }

        if ($w != null) {
            $this->db->where($w);
        }

        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}
