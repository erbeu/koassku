<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class App extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('keluhan_model', 'keluhan');
        $this->load->model('notif_model', 'notif');
        $this->load->model('kotakab_model', 'kotakab');
    }

    public function index()
    {
        if (!$this->session->is_loggedin || $this->session->role != 'pasien') {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }

        $q['keluhan.created_at'] = urldecode($this->input->get('q[waktu]', true));
        $q['subjek'] = urldecode($this->input->get('q[subjek]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().$this->router->fetch_module();
            $config['first_url'] = base_url().$this->router->fetch_module();
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->keluhan->total_rows($q, ['pasien' => $this->session->id_pasien]);
        $keluhan = $this->keluhan->get_limit_data($config['per_page'], $start, $q, ['pasien' => $this->session->id_pasien]);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = [
            'title' => 'Dasbor',
            'keluhan' => $keluhan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        ];
        
        $this->layout->render('dasbor', $data);
    }

    public function login()
    {
        if ($this->session->is_loggedin && $this->session->role == 'pasien') {
            redirect(site_url($this->router->fetch_module()));
        }

        if ($this->input->post('submit', true)) {
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $login = $this->pasien->login($email, $password);

            if ($login == 1) {
                redirect(site_url($this->router->fetch_module()));
            } elseif ($login == 2) {
                $this->session->set_flashdata('message', alert('danger', 'Username / password salah'));
            } elseif ($login == 3) {
                $this->session->set_flashdata('message', alert('danger', 'Akun anda belum aktif'));
            } else {
                $this->session->set_flashdata('message', alert('danger', 'Error'));
            }
        }

        $this->layout->set_layout('login');

        $data = [
            'title' => 'Login',
        ];

        $this->layout->render('login', $data);
    }

    public function daftar()
    {
        if ($this->session->is_loggedin && $this->session->role == 'pasien') {
            redirect(site_url($this->router->fetch_module()));
        }

        if ($this->input->post('submit', true)) {
            $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|is_unique[pasien.email]');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('nama_pasien', 'nama', 'trim|required');
            $this->form_validation->set_rules('no_hp', 'no hp', 'trim|numeric|required');
            $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
            $this->form_validation->set_rules('kotakab', 'kota/kab', 'trim|required');
            $this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
            $this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
            $this->form_validation->set_rules('gol_darah', 'gol darah', 'trim|required');
            $this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
            $this->form_validation->set_rules('status_nikah', 'status nikah', 'trim|required');
            $this->form_validation->set_rules('agama', 'agama', 'trim|required');
            $this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'trim|required');
            $this->form_validation->set_rules('pendidikan', 'pendidikan', 'trim|required');
            $this->form_validation->set_rules('alergi', 'alergi', 'trim');
            if (empty($_FILES['foto']['name'])) {
                $this->form_validation->set_rules('foto', 'foto', 'required');
            }

            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

            if ($this->form_validation->run()) {
                $config['upload_path'] = './_uploads/pasien/';
                $config['file_name'] = rand(1,999).'_'.md5($this->input->post('email', true)).'.'.pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = 2048;
                $this->load->library('upload', $config);

                $data = [
                    'email'         => $this->input->post('email', true),
                    'password'      => password_hash($this->input->post('password', true), PASSWORD_DEFAULT),
                    'token'         => substr(bin2hex(random_bytes(32)), 0, 32),
                    'nama_pasien'   => $this->input->post('nama_pasien', true),
                    'no_hp'         => $this->input->post('no_hp', true),
                    'alamat'        => $this->input->post('alamat', true),
                    'kotakab'       => $this->input->post('kotakab', true),
                    'tempat_lahir'  => $this->input->post('tempat_lahir', true),
                    'tanggal_lahir' => $this->input->post('tanggal_lahir', true),
                    'gol_darah'     => $this->input->post('gol_darah', true),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin', true),
                    'status_nikah'  => $this->input->post('status_nikah', true),
                    'agama'         => $this->input->post('agama', true),
                    'pekerjaan'     => $this->input->post('pekerjaan', true),
                    'pendidikan'    => $this->input->post('pendidikan', true),
                    'alergi'        => $this->input->post('alergi', true),
                    'foto'          => $config['file_name'],
                    'status'        => 0,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'soft_delete'   => 0,
                ];

                if ($this->upload->do_upload('foto')) {
                    $config['image_library']    = 'gd2';
                    $config['source_image']     = $config['upload_path'].$config['file_name'];
                    $config['width']            = 250;
                    $config['maintain_ratio']   = true;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $this->db->trans_begin();
                    $this->pasien->insert($data);

                    $this->load->library('email');
                    $this->email->from($this->config->item('app')['email'], $this->config->item('app')['name']);
                    $this->email->to($data['email']);
                    $this->email->subject('Konfirmasi Pendaftaran');

                    $email_message = $this->load->view('layouts/email/daftar', ['nama_pasien' => $data['nama_pasien'], 'token' => $data['token']], true);
                    $this->email->message($email_message);

                    if ($this->email->send() && $this->db->trans_status()) {
                        $this->db->trans_commit();
                        $this->session->set_flashdata('message', alert('success', 'Pendaftaran berhasil dilakukan. Silahkan cek email anda untuk mengaktifkan akun.'));
                        redirect(site_url($this->router->fetch_module().'/app/login/'));
                    } else {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('message', alert('danger', $this->email->print_debugger()));
                    }
                } else {
                    $this->session->set_flashdata('message', alert('danger', $this->upload->display_errors()));
                }
            }
        }

        $this->layout->set_layout('daftar');

        $data = [
            'title' => 'Pendaftaran Pasien',
            'load_css' => [
                'select2.min.css',
                'bootstrap-datetimepicker.min.css',
            ],
            'load_js' => [
                'select2.min.js',
                'bootstrap-datetimepicker.min.js',
            ],
            'load_script' => [
                '$(".datetimepicker").datetimepicker({
                    format : "YYYY-MM-DD HH:mm:ss"
                });
                $(".select2").select2();',
            ],
            'kotakab'       => $this->kotakab->get_all(),
            'jenis_kelamin' => $this->config->item('data_provider')['jenis_kelamin'],
            'gol_darah'     => $this->config->item('data_provider')['gol_darah'],
            'status_nikah'  => $this->config->item('data_provider')['status_nikah'],
            'agama'         => $this->config->item('data_provider')['agama'],
            'pendidikan'    => $this->config->item('data_provider')['pendidikan'],
        ];

        $this->layout->render('daftar', $data);
    }

    public function konfirmasi_pendaftaran($token)
    {
        $pasien = $this->pasien->get_by_token($token);
        if ($pasien != null) {
            $this->pasien->update($pasien->id_pasien, ['token' => '', 'status' => 1]);
            $this->session->set_flashdata('message', alert('success', 'Akun anda sudah aktif, silahkan login.'));

            $this->load->library('email');
            $this->email->from($this->config->item('app')['email'], $this->config->item('app')['name']);
            $this->email->to($pasien->email);
            $this->email->subject('Selamat Datang');

            $email_message = $this->load->view('layouts/email/selamat_datang', ['nama_pasien' => $pasien->nama_pasien], true);
            $this->email->message($email_message);
            $this->email->send();
        } else {
            $this->session->set_flashdata('message', alert('danger', 'User tidak ditemukan.'));
        }

        redirect(site_url($this->router->fetch_module().'/app/login/'));
    }

    public function logout()
    {
        session_destroy();
        redirect(site_url($this->router->fetch_module().'/app/login/'));
    }
    
    public function baca_notif($id)
    {
        if (!$this->session->is_loggedin || $this->session->role != 'pasien') {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }

        $notif = $this->notif->get_by_id($id);
        if ($notif->pasien == $this->session->id_pasien && $notif->penerima = 3) {
            $this->notif->update($id, ['dibaca' => 1]);
            redirect(site_url($this->router->fetch_module().'/'.$notif->uri));
        }

        redirect(site_url($this->router->fetch_module()));
    }
}
