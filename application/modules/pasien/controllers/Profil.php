<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profil extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('keluhan_model', 'keluhan');
        $this->load->model('kotakab_model', 'kotakab');

        if (!$this->session->is_loggedin || $this->session->role != 'pasien') {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }
    }

    public function index()
    {
        $pasien = $this->pasien->get_by_id_ori($this->session->id_pasien);

        $data = [
            'title'         => 'Profil',
            'pasien'        => $pasien,
            'kotakab'       => $this->kotakab->get_all('array'),
            'jenis_kelamin' => $this->config->item('data_provider')['jenis_kelamin'],
            'gol_darah'     => $this->config->item('data_provider')['gol_darah'],
            'status_nikah'  => $this->config->item('data_provider')['status_nikah'],
            'agama'         => $this->config->item('data_provider')['agama'],
            'pendidikan'    => $this->config->item('data_provider')['pendidikan'],
        ];

        $this->layout->render('main', $data);
    }

    public function edit()
    {
        if ($this->input->post('submit', true)) {
            $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|callback_email_check');
            $this->form_validation->set_rules('nama_pasien', 'nama', 'trim|required');
            $this->form_validation->set_rules('no_hp', 'no hp', 'trim|numeric|required');
            $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
            $this->form_validation->set_rules('kotakab', 'kota/kab', 'trim|required');
            $this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
            $this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
            $this->form_validation->set_rules('gol_darah', 'gol darah', 'trim|required');
            $this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
            $this->form_validation->set_rules('status_nikah', 'status nikah', 'trim|required');
            $this->form_validation->set_rules('agama', 'agama', 'trim|required');
            $this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'trim|required');
            $this->form_validation->set_rules('pendidikan', 'pendidikan', 'trim|required');
            $this->form_validation->set_rules('alergi', 'alergi', 'trim|required');

            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

            if ($this->form_validation->run()) {
                $error = 0;

                $data = [
                    'email'         => $this->input->post('email', true),
                    'nama_pasien'   => $this->input->post('nama_pasien', true),
                    'no_hp'         => $this->input->post('no_hp', true),
                    'alamat'        => $this->input->post('alamat', true),
                    'kotakab'       => $this->input->post('kotakab', true),
                    'tempat_lahir'  => $this->input->post('tempat_lahir', true),
                    'tanggal_lahir' => $this->input->post('tanggal_lahir', true),
                    'gol_darah'     => $this->input->post('gol_darah', true),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin', true),
                    'status_nikah'  => $this->input->post('status_nikah', true),
                    'agama'         => $this->input->post('agama', true),
                    'pekerjaan'     => $this->input->post('pekerjaan', true),
                    'pendidikan'    => $this->input->post('pendidikan', true),
                    'alergi'        => $this->input->post('alergi', true),
                    'status'        => 1,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'soft_delete'   => 0,
                ];

                if (!empty($this->input->post('password', true))) {
                    $data['password'] = password_hash($this->input->post('password', true), PASSWORD_DEFAULT);
                }

                if (file_exists($_FILES['foto']['tmp_name']) && is_uploaded_file($_FILES['foto']['tmp_name'])) {
                    $config['upload_path'] = './_uploads/pasien/';
                    $config['file_name'] = $this->input->post('nama_foto', true).'.'.pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['overwrite'] = true;
                    $config['max_size'] = 1024;
                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('foto')) {
                        $config['image_library']    = 'gd2';
                        $config['source_image']     = $config['upload_path'].$config['file_name'];
                        $config['width']            = 250;
                        $config['maintain_ratio']   = true;
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();

                        $data['foto'] = $config['file_name'];
                    } else {
                        $this->session->set_flashdata('message', alert('danger', $this->upload->display_errors()));
                        $error++;
                    }
                }

                if ($error == 0) {
                    $this->pasien->update($this->session->id_pasien, $data);
                    $this->session->set_flashdata('message', alert('success', 'Profil berhasil diedit.'));
                    redirect(site_url($this->router->fetch_module().'/profil/'));
                } else {
                    $this->session->set_flashdata('message', alert('danger', $this->upload->display_errors()));
                }
            }
        }

        $pasien = $this->pasien->get_by_id_ori($this->session->id_pasien);

        $data = [
            'title' => 'Edit Profil',
            'pasien' => $pasien,
            'load_css' => [
                'select2.min.css',
                'bootstrap-datetimepicker.min.css',
            ],
            'load_js' => [
                'select2.min.js',
                'bootstrap-datetimepicker.min.js',
            ],
            'load_script' => [
                '$(".datetimepicker").datetimepicker({
                    format : "YYYY-MM-DD"
                });
                $(".select2").select2();',
            ],
            'kotakab'       => $this->kotakab->get_all(),
            'jenis_kelamin' => $this->config->item('data_provider')['jenis_kelamin'],
            'gol_darah'     => $this->config->item('data_provider')['gol_darah'],
            'status_nikah'  => $this->config->item('data_provider')['status_nikah'],
            'agama'         => $this->config->item('data_provider')['agama'],
            'pendidikan'    => $this->config->item('data_provider')['pendidikan'],
        ];

        $this->layout->render('edit', $data);
    }

    public function email_check($email)
    {
        if ($this->pasien->search_email($email) != 1) {
            $this->form_validation->set_message('email_check', 'The {field} field must contain a unique value.');
            return false;
        } else {
            return true;
        }
    }
}
