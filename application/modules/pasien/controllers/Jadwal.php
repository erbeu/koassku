<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('jadwal_model', 'jadwal');
        $this->load->model('keluhan_model', 'keluhan');
        $this->load->model('konsultan_model', 'konsultan');

        if (!$this->session->is_loggedin || $this->session->role != 'pasien') {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }
    }

    public function index()
    {
        $q['waktu'] = urldecode($this->input->get('q[waktu]', true));
        $q['judul'] = urldecode($this->input->get('q[judul]', true));
        $q['nama_konsultan'] = urldecode($this->input->get('q[nama_konsultan]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().$this->router->fetch_module();
            $config['first_url'] = base_url().$this->router->fetch_module();
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->jadwal->total_rows($q, ['pasien.id_pasien' => $this->session->id_pasien]);
        $jadwal = $this->jadwal->get_limit_data($config['per_page'], $start, $q, ['pasien.id_pasien' => $this->session->id_pasien]);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = [
            'title' => 'Jadwal',
            'pasien' => $this->pasien->get_by_id($this->session->id_pasien),
            'jadwal' => $jadwal,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'load_css' => [
                'fullcalendar.min.css',
            ],
            'load_js' => [
                'fullcalendar.min.js',
            ],
        ];
        
        $this->layout->render('main', $data);
    }

    public function detail($id_jadwal)
    {
        $jadwal = $this->jadwal->get_by_id($id_jadwal);
        $keluhan = $this->keluhan->get_by_id($jadwal->keluhan);
        $konsultan = $this->konsultan->get_by_id($keluhan->konsultan);
        $pasien = $this->pasien->get_by_id($this->session->id_pasien);

        $data = [
            'title' => 'Detail Jadwal',
            'jadwal' => $jadwal,
            'keluhan' => $keluhan,
            'konsultan' => $konsultan,
            'pasien' => $pasien,
        ];

        $this->layout->render('detail', $data);
    }
}
