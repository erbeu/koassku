<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Keluhan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('keluhan_model', 'keluhan');
        $this->load->model('keluhan_detail_model', 'keluhan_detail');
        $this->load->model('jadwal_model', 'jadwal');
        $this->load->model('notif_model', 'notif');

        if (!$this->session->is_loggedin || $this->session->role != 'pasien') {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }
    }

    public function index()
    {
        if (!$this->session->is_loggedin && $this->session->role != 'pasien') {
            redirect(site_url($this->router->fetch_module().'/app/login/'));
        }

        $q['keluhan.created_at'] = urldecode($this->input->get('q[waktu]', true));
        $q['subjek'] = urldecode($this->input->get('q[subjek]', true));
        $start = intval($this->input->get('start'));

        if ($q != '') {
            $config['base_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
            $config['first_url'] = base_url().$this->router->fetch_module().'?'.http_build_query(['q' => $q]);
        } else {
            $config['base_url'] = base_url().$this->router->fetch_module();
            $config['first_url'] = base_url().$this->router->fetch_module();
        }

        $config['per_page'] = 5;
        $config['page_query_string'] = true;
        $config['total_rows'] = $this->keluhan->total_rows($q, ['pasien' => $this->session->id_pasien]);
        $keluhan = $this->keluhan->get_limit_data($config['per_page'], $start, $q, ['pasien' => $this->session->id_pasien]);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = [
            'title' => 'Keluhan',
            'pasien' => $this->pasien->get_by_id($this->session->id_pasien),
            'keluhan' => $keluhan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        ];
        
        $this->layout->render('main', $data);
    }

    public function detail($id_keluhan)
    {
        $keluhan = $this->keluhan->get_by_id($id_keluhan);
        $keluhan_detail = $this->keluhan_detail->get_by_keluhan($id_keluhan);

        $jadwal = $this->jadwal->get_limit_data(1, 0, null, 'jadwal.keluhan = '.$id_keluhan);

        $data = [
            'title' => 'Detail Keluhan',
            'pasien' => $this->pasien->get_by_id($this->session->id_pasien),
            'keluhan' => $keluhan,
            'keluhan_detail' => $keluhan_detail,
            'jadwal' => $jadwal,
            'load_css' => [
                'select2.min.css',
                'bootstrap-datetimepicker.min.css',
            ],
            'load_js' => [
                'select2.min.js',
                'bootstrap-datetimepicker.min.js',
            ],
            'load_script' => [
                '$(".message-time").each(function () {
                    $(this).html(moment($(this).html(), "YYYY-MM-DD HH:mm:ss").fromNow());
                });
                $(".datetimepicker").datetimepicker({
                    format : "YYYY-MM-DD HH:mm:ss"
                });
                $(".select2").select2();',
                $this->input->post('submit', true) ? '$("#myModal").modal("show")' : '',
            ],
        ];

        $this->layout->render('detail', $data);
    }

    public function buat()
    {
        $this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
        $this->form_validation->set_rules('usia', 'usia', 'trim|required');
        $this->form_validation->set_rules('status_nikah', 'status nikah', 'trim|required');
        $this->form_validation->set_rules('bagian_sakit', 'bagian sakit', 'trim|required');
        $this->form_validation->set_rules('lama_sakit', 'lama sakit', 'trim|required');
        $this->form_validation->set_rules('obat_kumur', 'obat kumur', 'trim|required');
        $this->form_validation->set_rules('jenis_bulu_sikat', 'jenis bulu sikat', 'trim|required');
        $this->form_validation->set_rules('subjek', 'subjek', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run()) {
            $error = 0;
            $foto_gigi = null;

            if (file_exists($_FILES['foto_gigi']['tmp_name']) && is_uploaded_file($_FILES['foto_gigi']['tmp_name'])) {
                $config['upload_path'] = './_uploads/gigi/';
                $config['file_name'] = date('YmdHis').'_'
                    .rand(1,999).'_'
                    .md5($this->input->post('subjek', true)).'.'
                    .pathinfo($_FILES['foto_gigi']['name'], PATHINFO_EXTENSION);
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = 2048;
                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto_gigi')) {
                    $foto_gigi = $config['file_name'];
                } else {
                    $this->session->set_flashdata('message', alert('danger', $this->upload->display_errors()));
                    $error++;
                }
            }

            if ($error == 0) {
                $data = [
                    'pasien'            => $this->session->id_pasien,
                    'konsultan'         => null,
                    'jenis_kelamin'     => $this->input->post('jenis_kelamin', true),
                    'usia'              => $this->input->post('usia', true),
                    'status_nikah'      => $this->input->post('status_nikah', true),
                    'bagian_sakit'      => $this->input->post('bagian_sakit', true),
                    'lama_sakit'        => $this->input->post('lama_sakit', true),
                    'obat_kumur'        => $this->input->post('obat_kumur', true),
                    'jenis_bulu_sikat'  => $this->input->post('jenis_bulu_sikat', true),
                    'subjek'            => $this->input->post('subjek', true),
                    'deskripsi'         => $this->input->post('deskripsi', true),
                    'foto_gigi'         => $foto_gigi,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'soft_delete'       => 0,
                ];
                $id_keluhan = $this->keluhan->insert($data);

                $this->session->set_flashdata('message', alert('success', 'Keluhan berhasil dibuat.'));
                redirect(site_url($this->router->fetch_module().'/keluhan/detail/'.$id_keluhan));
            }
        }

        $data = [
            'title' => 'Buat Keluhan',
            'pasien' => $this->pasien->get_by_id($this->session->id_pasien),
            'load_css' => [
                'select2.min.css',
            ],
            'load_js' => [
                'select2.min.js',
            ],
            'load_script' => [
                '$(".select2").select2();',
            ],
            'jenis_kelamin'     => $this->config->item('data_provider')['jenis_kelamin'],
            'usia'              => $this->config->item('data_provider')['usia'],
            'status_nikah'      => $this->config->item('data_provider')['status_nikah'],
            'ya_tidak'          => $this->config->item('data_provider')['ya_tidak'],
            'jenis_bulu_sikat'  => $this->config->item('data_provider')['jenis_bulu_sikat'],
        ];

        $this->layout->render('buat', $data);
    }

    public function buat_jadwal($id_keluhan)
    {
        if ($this->input->post('submit', true)) {
            $this->form_validation->set_rules('judul', 'judul', 'trim|required');
            $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
            $this->form_validation->set_rules('waktu', 'waktu', 'trim|required');
            $this->form_validation->set_rules('lokasi', 'lokasi', 'trim|required');

            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

            if ($this->form_validation->run()) {
                $data = [
                    'judul' => $this->input->post('judul', true),
                    'deskripsi' => $this->input->post('deskripsi', true),
                    'waktu' => $this->input->post('waktu', true),
                    'lokasi' => $this->input->post('lokasi', true),
                    'pasien' => $this->session->id_pasien,
                    'keluhan' => $id_keluhan,
                    'created_at' => date('Y-m-d H:i:s'),
                    'hapus' => 0,
                ];

                $this->jadwal->insert($data);
                $this->session->set_flashdata('message', alert('success', 'Jadwal berhasil disimpan.'));
                redirect(site_url($this->router->fetch_module().'/keluhan/detail/'.$id_keluhan));
            } else {
                $this->detail($id_keluhan);
            }
        }
    }

    public function balas($id_keluhan)
    {
        $this->form_validation->set_rules('isi', 'isi pesan', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == false) {
            $this->detail($id_keluhan);
        } else {
            $this->keluhan_detail->insert([
                'keluhan'       => $id_keluhan,
                'pengirim'      => 3,
                'isi'           => nl2br($this->input->post('isi', true)),
                'created_at'    => date('Y-m-d H:i:s'),
                'soft_delete'   => 0,
            ]);

            $keluhan = $this->keluhan->get_by_id($id_keluhan);
            $this->notif->insert([
                'konsultan'     => $keluhan->konsultan,
                'pasien'        => $this->session->id_pasien,
                'penerima'      => 2,
                'isi'           => $this->session->nama_pasien.' mengirimkan pesan.',
                'uri'           => 'keluhan/detail/'.$id_keluhan,
                'created_at'    => date('Y-m-d H:i:s'),
                'dibaca'        => 0,
            ]);

            $this->session->set_flashdata('message', alert('success', 'Pesan Sudah Terkirim'));
            redirect(site_url($this->router->fetch_module().'/keluhan/detail/'.$id_keluhan));
        }
    }

    public function ambil($id_keluhan)
    {
        $keluhan = $this->keluhan->get_by_id($id_keluhan);

        if ($keluhan->konsultan == null) {
            $this->keluhan->update($id_keluhan, ['konsultan' => $this->session->id_konsultan]);
            $this->konsultan->decrease_point($this->session->id_konsultan, 1);
            redirect(site_url($this->router->fetch_module().'/keluhan/detail/'.$id_keluhan));
        } else {
            redirect(site_url($this->router->fetch_module()));
        }
    }

    public function _rules()
    {
    }
}
