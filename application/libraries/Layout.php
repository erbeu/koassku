<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Layout
{
    private $layout_folder = 'layouts';
    private $layout = 'default';
    private $obj;

    public function __construct()
    {
        $this->obj = &get_instance();
    }

    public function set_layout($layout)
    {
        $this->layout = $layout;
    }

    public function set_layout_folder($layout_folder)
    {
        $this->layout_folder = $layout_folder;
    }

    public function render($view, $data)
    {
        $loaded_data = array();
        $loaded_data['view'] = $this->obj->router->fetch_module().'/'.$this->obj->router->fetch_class().'/'.$view;
        $loaded_data['data'] = $data;

        $layout_path = '/'.$this->layout_folder.'/'.$this->layout;
        $this->obj->load->view($layout_path, $loaded_data);
    }
}
