<?php
$config['app'] = [
    'name' => 'Koassku',
    'version' => 'v0.7.0',

    'email' => 'no-reply@koassku.id',
];

$config['data_provider'] = [
    'bulan' => [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember',
    ],
    'tipe_user' => [
        1 => 'admin',
        'konsultan',
        'pasien',
    ],
    'status' => [
        1 => 'aktif',
        'non aktif',
    ],
    'jenis_kelamin' => [
        1 => 'Laki - Laki',
        'Perempuan',
    ],
    'usia' => [
        1 => 'Anak - Anak',
        'Dewasa',
    ],
    'pendidikan' => [
        1 => 'SD',
        'SMP',
        'SMA',
        'D1',
        'D2',
        'D3',
        'D4',
        'S1',
        'S2',
        'S3',
    ],
    'jenis_bulu_sikat' => [
        1 => 'Sangat Lembut',
        'Lembut',
        'Sedang',
        'Keras',
    ],
    'ya_tidak' => [
        1 => 'Ya',
        'Tidak',
    ],
    'agama' => [
        1 => 'Islam',
        'Katolik',
        'Protestan',
        'Hindu',
        'Budha',
    ],
    'gol_darah' => [
        1 => 'A',
        'B',
        'AB',
        'O',
    ],
    'status_nikah' => [
        1 => 'Belum Menikah',
        'Sudah Menikah',
        'Duda/Janda',
    ],
];